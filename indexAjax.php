<!-- Estas lineas es para implementar tooltip (Peque�as etiquetas descriptivas de botones) -->
<script type="text/javascript">
	$(function () 
	{
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>   
<?php  
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();
    require "logica/Usuario.php";
    require "logica/Carta.php";
    require "logica/Plato.php";
    require "logica/Ingrediente.php";
    require "logica/Ingrediente_Plato.php";
    require "logica/Plato_Carta.php";
    require "logica/Region.php";
    require "logica/Orden.php";
    require "logica/Bandeja.php";
    require "logica/Proceso.php";
    require "logica/Log.php";
    require "persistencia/Conexion.php";
    
    //Zona horaria en colombia:
    date_default_timezone_set('America/Bogota'); 
    
    //Desencriptamos el pid para que lo pueda leer:
    $pid = base64_decode($_GET["pid"]);
    include $pid;
    
?>