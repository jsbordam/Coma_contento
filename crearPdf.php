<?php
    require_once "dompdf/autoload.inc.php";
    use Dompdf\Dompdf;
    $dompdf = new Dompdf();

    //Iniciamos un output buffer
    ob_start(); 

        //Llamamos el archivo que contiene el html y definimos los valores de la orden y el id del usuario:
        $_GET["idOrden"] = $_GET["idOrden"];
        $_GET["idUsu"] = $_GET["idUsu"];

        require_once("datos.php");

        #require_once("datos.php"); 

        //Y agregamos todo lo que se capturo con ob_start() para que sea capturado por DOMPDF
        $dompdf->load_html(ob_get_clean());

        //Damos propiedades al papel de salida
        $dompdf->set_paper("letter", "portrait");

        //Renderizamos, es decir convertir de html a pdf
        $dompdf->render();

    ob_end_clean(); //Finalizamos el output

    $dompdf->stream("Proceso.pdf");
?>

