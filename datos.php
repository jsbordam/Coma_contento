<?php 
    //Para importar las clases que necesito usar:
    require "logica/Usuario.php";
    require "logica/Carta.php";
    require "logica/Plato.php";
    require "logica/Ingrediente.php";
    require "logica/Ingrediente_Plato.php";
    require "logica/Plato_Carta.php";
    require "logica/Region.php";
    require "logica/Orden.php";
    require "logica/Bandeja.php";
    require "logica/Proceso.php";
    require "persistencia/Conexion.php";

    //Recolección datos cliente:

    $id = $_GET["idUsu"]; //Id del cliente al que se le genera reporte
    $cliente = new Usuario ($id);
    $cliente -> consultar();

    //Datos de la orden:

    $idOrden = $_GET["idOrden"]; //Id de la orden que se reporta
    $orden = new Orden ($idOrden);
    $orden -> consultarUno();

    //Datos para recepcion de la orden:

    $proceso = new Proceso ("", $idOrden);
    $proceso -> consultar(3); //Consulto el registro de estado 3 en la orden

    //Luego busco los datos del chef que atendio esa orden:

    $chef = new Usuario ($proceso -> getIdUsuario());
    $chef -> consultar();

    //Datos para preparación de la orden:

    $proceso2 = new Proceso ("", $idOrden);
    $proceso2 -> consultar(4); //Consulto el registro de estado 4 en la orden

    //Datos de los ingredientes empelados en la preparacion de la orden:
    $ingrediente = new Ingrediente();
    $arreglo = $ingrediente -> consultarIngre($idOrden);

    //Datos para entrega de la orden:

    $proceso3 = new Proceso ("", $idOrden);
    $proceso3 -> consultar(6); //Consulto el registro de estado 6 en la orden
 
?>

<html>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">

    <div class="row justify-content-xl-center">
        <div class="col-xl-12">
            <h2>REPORTE DEL PROCESO DE ORDEN <mark>#<?php echo $orden -> getIdOrden()?></mark></h2>
        </div>
    </div>
    <div aling="text-center">_______________________________________________________________________________</div>
</head>

<body>
    <br><br><br>
    <h3><b>Datos del Cliente:</b></h3>
    <br><br>
    <div class="row">
        <div class="col-md-12" align="center">
            <img src="<?php echo $cliente->getFoto()?>" width="200px" height="200px">
        </div>
    </div>

    <table class="table table-warning table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Id</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Nombre</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Apellido</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Num Docto</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Correo</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $cliente->getIdUsuario() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $cliente->getNombre() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $cliente->getApellido() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $cliente->getNum_docto() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $cliente->getCorreo() . "</font></h5></td>";
                    echo "</tr>";            	      	                   	                         	
                ?>
        </tbody>
    </table>


    <br><br>
    <h4><b>Actividad N° 1: RESERVA</b></h4>
    <br><br>

    <table class="table table-info table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Id</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Restaurante</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Fecha</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Hora</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Personas</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>26</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>Central Salitre</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>03-03-2021</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'></font>16:00</h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'></font>Para 3</h5></td>";
                    echo "</tr>";            	      	                   	                         	
                ?>
        </tbody>
    </table>

    <br><br><br><br>
    <h4><b>Actividad N° 2: REGISTRO DE LLEGADA AL RESTAURANTE</b></h4>
    <br><br>

    <table class="table table-warning table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Fecha</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Hora</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>03-03-2021</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>15:50</font></h5></td>";
                    echo "</tr>";            	      	                   	                         	
                ?>
        </tbody>
    </table>


    <br><br>
    <h4><b>Actividad N° 3: SOLICITUD DE LA ORDEN</b></h4>
    <br><br>

    <table class="table table-success table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Codigo</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Fecha y Hora</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Valor Total</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Observaciones</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getIdOrden() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getFecha_hora() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getValor_total() . "</font></h5></td>";
                        
                        if ($orden->getObs() == "")
                        {
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>No hay</font></h5></td>";
                        }
                        else
                        {
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getObs() . "</font></h5></td>";
                        }
                    echo "</tr>";   	      	                   	                         	
                ?>
        </tbody>
    </table>

    <br><br>
    <h4><b>Actividad N° 4: RECEPCIÓN DE LA ORDEN</b></h4>
    <br>
    <h5>Información sobre el chef que recibio la orden:</h5>
    <br><br>

    <div class="row">
        <div class="col-md-12" align="center">
            <img src="<?php echo $chef->getFoto()?>" width="200px" height="200px">
        </div>
    </div>
    <br><br>

    <table class="table table-info table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Id</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Nombre</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Apellido</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Num Docto</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Correo</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $chef->getIdUsuario() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $chef->getNombre() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $chef->getApellido() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $chef->getNum_docto() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $chef->getCorreo() . "</font></h5></td>";
                    echo "</tr>";            	      	                   	                         	
                ?>
        </tbody>
    </table>

    <br><br><br>
    <h5>Información sobre la recepción:</h5>
    <br><br>

    <table class="table table-danger table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Codigo</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Orden</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Estado</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Fecha y Hora</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso->getIdProceso() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso->getIdOrden() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>Recibida</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso->getFecha_hora() . "</font></h5></td>";       
                    echo "</tr>";   	      	                   	                         	
                ?>
        </tbody>
    </table>

    <br><br>
    <h4><b>Actividad N° 5: PREPARACIÓN DE LA ORDEN</b></h4>
    <br><br>

    <table class="table table-danger table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Codigo</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Orden</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Estado</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Fecha y Hora</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Observaciones</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso2->getIdProceso() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso2->getIdOrden() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>Preparada</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso2->getFecha_hora() . "</font></h5></td>";       
                        if ($proceso2->getObs() == "")
                        {
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>No hay</font></h5></td>";
                        }
                        else
                        {
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso2->getObs() . "</font></h5></td>";
                        }      
                    echo "</tr>";   	      	                   	                         	
                ?>
        </tbody>
    </table>

    <br><br><br><br><br><br><br><br>
    <h5>Ingredientes empleados en la preparación de la orden:</h5>
    <br><br>
    <table class="table table-warning table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Codigo</font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Nombre</font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Cantidad</font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Medida</font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($arreglo as $i)
                {                  	   
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $i->getIdIngrediente() . "</font></h5></td>";             	        
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $i->getNombre() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $i->getCantidad() . "</font></h5></td>";
                        
                        //Para unidad de medida:
                        if ($i->getUnidad_medida() == 1) 
                        {
                            $medida = "Libras";
                        }
                        else if ($i->getUnidad_medida() == 2)
                        {
                            $medida = "Kilos";
                        }
                        else if ($i->getUnidad_medida() == 3)
                        {
                            $medida = "Tazas";
                        }
                        else if ($i->getUnidad_medida() == 4)
                        {
                            $medida = "Unidades";
                        }
                        else if ($i->getUnidad_medida() == 5)
                        {
                            $medida = "Gramos";
                        }
                        else 
                        {
                            $medida = "Cucharadas";
                        }
                        
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $medida . "</font></h5></td>";
                    echo "</tr>";           	   
                }              	
            ?>
        </tbody>
    </table>

    <br><br>
    <h4><b>Actividad N° 6: ENTREGA DE LA ORDEN</b></h4>
    <br><br>

    <table class="table table-danger table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Codigo</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Orden</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Estado</b></font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='4' color='black'><b>Fecha y Hora</b></font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                echo "<tr>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso3->getIdProceso() . "</font></h5></td>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso3->getIdOrden() . "</font></h5></td>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>Entregada</font></h5></td>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $proceso3->getFecha_hora() . "</font></h5></td>";            
                echo "</tr>";   	      	                   	                         	
            ?>
        </tbody>
    </table>

</body>

</html>