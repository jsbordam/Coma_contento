<?php  
    if (isset($_GET["eliminar"])) //Muestra el modal de eliminar
    {  
?>

<div class="modal-header">
	<font face="Alergan" size="5" color="red">ATENCIÓN <i class="fas fa-exclamation-triangle"></i></font>
</div>
<div class="modal-body">
	<h4 class="modal-title">Seguro que quieres cancelar esta orden?</h4>
</div>
<div class="modal-footer">
	<a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&eliminar" class="btn btn-danger modal_btn3 btn-block">CONFIRMAR</a>
	<button type="button" class="btn btn-dark modal_btn4 btn-block" data-dismiss="modal">CANCELAR</button>
</div>

<?php 
    }
    else 
    {
        
    }
?>