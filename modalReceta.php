<?php 
    require "logica/Plato.php";
    require "logica/Ingrediente.php";
    require "persistencia/Conexion.php";


    $idPlato = $_GET["idPla"];

    //Consultamos la receta de ese plato:
    $plato = new Plato ($idPlato);
    $plato -> consultar();

    //Consultamos las lista de ingredientes:
    $ingrediente = new Ingrediente();
    $arreglo = $ingrediente -> consultar ($idPlato);

?>

<!-- Modal body -->
<div class="modal-body">
    <div class="col-md-12">
        <h1 align="center">
            <font face="cursive" size="6" color="white">
                Receta del plato
            </font>
        </h1>
    </div>

    <hr class="divider hr-xl-center-0 bg-mantis offset-top-30">

    <p style="text-align: justify;">
        <font color="Red"><i class="fas fa-utensils"></i> Descripcion Platillo:</font>
        <br><br>
        <?php echo $plato -> getDescripcion();?>
    </p>

    <br>

    <p style="text-align: justify;">
        <font color="Red"><i class="fas fa-utensils"></i> Descripcion Receta:</font>
        <br><br>
        <?php echo $plato -> getDescripcion_receta();?>
    </p>

    <br>

    <p>
        <font color="Red"><i class="fas fa-mortar-pestle"></i> Categoria:</font>
        <br><br>
        <?php 
            if ($plato -> getIdCategoria() == 1)
            {
                $categoria = "Sopas";
            }
            else if ($plato -> getIdCategoria() == 2)
            {
                $categoria = "Carnes";
            }
            else if ($plato -> getIdCategoria() == 3)
            {
                $categoria = "Pollo";
            }
            else if ($plato -> getIdCategoria() == 4)
            {
                $categoria = "Postres";
            }
            else if ($plato -> getIdCategoria() == 5)
            {
                $categoria = "Arroces";
            }
            else
            {
                $categoria = "Pescados";
            }

            echo $categoria;
        ?>
    </p>

    <br>

    <p>
        <font color="Red"><i class="fas fa-list-ol"></i></i> Complejidad:</font>
        <br><br>
        <?php 
            if ($plato -> getIdComplejidad() == 1)
            {
                $comple = "Nivel Bajo";
            }
            else if ($plato -> getIdComplejidad() == 2)
            {
                $comple = "Nivel Basico";
            }
            else if ($plato -> getIdComplejidad() == 3)
            {
                $comple = "Nivel Medio";
            }
            else
            {
                $comple = "Nivel Alto";
            }

            echo $comple;
        ?>
    </p>

    <br>

    <p>
        <font color="Red"><i class="fas fa-carrot"></i> Ingredientes:</font>
        <br>
    <table class="table table-warning table-hover">
        <thead>
            <tr>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Codigo</font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Nombre</font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Cantidad</font>
                    </h5>
                </th>
                <th class="text-center">
                    <h5>
                        <font face='Arial Black' size='3' color='black'>Medida</font>
                    </h5>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($arreglo as $i)
                {                  	   
                    echo "<tr>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $i->getIdIngrediente() . "</font></h5></td>";             	        
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $i->getNombre() . "</font></h5></td>";
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $i->getCantidad() . "</font></h5></td>";
                        
                        //Para unidad de medida:
                        if ($i->getUnidad_medida() == 1) 
                        {
                            $medida = "Libras";
                        }
                        else if ($i->getUnidad_medida() == 2)
                        {
                            $medida = "Kilos";
                        }
                        else if ($i->getUnidad_medida() == 3)
                        {
                            $medida = "Tazas";
                        }
                        else if ($i->getUnidad_medida() == 4)
                        {
                            $medida = "Unidades";
                        }
                        else if ($i->getUnidad_medida() == 5)
                        {
                            $medida = "Gramos";
                        }
                        else 
                        {
                            $medida = "Cucharadas";
                        }
                        
                        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $medida . "</font></h5></td>";
                    echo "</tr>";           	   
                }              	
            ?>
        </tbody>
    </table>
    </p>
</div>
<div class="col-md-12">
    <div align="center">
        <a href="#">
            <font size="8" color="red"><i class="far fa-times-circle" data-dismiss="modal"></i></font>
        </a>
    </div>
</div>