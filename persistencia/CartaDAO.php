<?php
    class CartaDAO
    {
        private $idCarta;
        private $nombre;
        private $vigencia;
        private $estado;
        
        
        //Constructor:
        
        function CartaDAO ($pIdCarta="", $pNombre="", $pVigencia="", $pEstado="")
        {
            $this -> idCarta = $pIdCarta;
            $this -> nombre = $pNombre;
            $this -> vigencia = $pVigencia;
            $this -> estado = $pEstado;
        }
        
        //Metodos para las consultas en la BD:
        
        //Para crear:
        function crear()
        {
            return "INSERT into carta (idCarta, nombre, vigencia)
                    VALUES ('" . $this -> idCarta . "', '" . $this -> nombre . "', '" . $this -> vigencia . "')";
        }
        
        //Para consultar una:
        function consultar()
        {
            return "SELECT idCarta, nombre, vigencia, estado
                    FROM carta
                    WHERE nombre = '" . $this -> nombre . "' OR idCarta = '" . $this -> idCarta . "'";
        }     
        
        //Para consultar todas:
        function consultarTodos()
        {
            return "SELECT idCarta, nombre, vigencia, estado
                    FROM carta";
        } 

        //Para editar:
        function editar()
        {
            return "UPDATE carta
                    SET nombre = '". $this -> nombre . "', vigencia = '". $this -> vigencia . "'
                    WHERE idCarta = '" . $this -> idCarta . "'";
        }

        //Para actualizar la carta activa:
        function activa()
        {
            return "UPDATE carta
                    SET estado = '". $this -> estado . "'
                    WHERE idCarta = '" . $this -> idCarta . "'";
        }

        //Para consultar cuantos platos hay en cada carta:
        function platosEnCarta()
        {
            return "SELECT c.nombre, count(p.idPlato_FK) AS total
                    FROM carta c LEFT JOIN plato_carta p on (c.idCarta = p.idCarta_FK)
                    group by c.nombre
                    order by total desc";
        }
    }
?>
