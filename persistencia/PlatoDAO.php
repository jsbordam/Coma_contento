<?php
    class PlatoDAO
    {
        private $idPlato;
        private $nombre;
        private $descripcion;
        private $foto;
        private $precio;
        private $descripcion_receta;
        private $idCategoria;
        private $idRegion;
        private $idComplejidad;
        
        
        //Constructor:
        
        function PlatoDAO ($pIdPlato="", $pNombre="", $pDescripcion="", $pFoto="", $pPrecio="", $pDescripcion_receta="", $pIdCategoria="", $pIdRegion="", $pIdComplejidad="")
        {
            $this -> idPlato = $pIdPlato;
            $this -> nombre = $pNombre;
            $this -> descripcion = $pDescripcion;
            $this -> foto = $pFoto;
            $this -> precio = $pPrecio;
            $this -> descripcion_receta = $pDescripcion_receta;
            $this -> idComplejidad = $pIdComplejidad;
            $this -> idCategoria = $pIdCategoria;
            $this -> idRegion = $pIdRegion;
        }
        
        //Metodos para las consultas en la BD:
        
        //Para crear:
        function crear()
        {
            return "INSERT into plato (idPlato, nombre, descripcion, foto, precio, descripcion_receta, idCategoria_FK, idRegion_FK, idComplejidad_FK)
                    VALUES ('" . $this -> idPlato . "', '" . $this -> nombre . "', '" . $this -> descripcion . "', '" . $this -> foto . "', '" . $this -> precio . "', '" . $this -> descripcion_receta . "', '" . $this -> idCategoria . "', '" . $this -> idRegion . "', '" . $this -> idComplejidad . "')";
        }
        
        //Para consultar uno:
        function consultar()
        {
            return "SELECT idPlato, nombre, descripcion, foto, precio, descripcion_receta, idCategoria_FK, idRegion_FK, idComplejidad_FK
                    FROM plato
                    WHERE idPlato = '" . $this -> idPlato . "'";
        }  
        
        //Para consultar todos los platos de una carta unicamente:
        function consultarTodos($idCar)
        {
            //Consulta compleja o subconsulta:
            return "SELECT idPlato, nombre, descripcion, foto, precio, descripcion_receta, idCategoria_FK, idRegion_FK, idComplejidad_FK
                    FROM plato
                    WHERE idPlato IN (SELECT idPlato_FK
                                      FROM plato_carta
                                      WHERE idCarta_FK = '" . $idCar . "')";
        }

        //Para consultar el ultimo id que se acaba de ingresar:
        function ultimoId()
        {
            return "SELECT MAX(idPlato)
                    FROM plato";
        }

        //Para contar el numero de platos de una region y de una carta especifica:
        function contarPla($idCarta)
        {
            return "SELECT DISTINCT idPlato
                    FROM plato, plato_carta
                    WHERE idRegion_FK = '" . $this -> idRegion . "' AND idCarta_FK = '" . $idCarta . "' AND idPlato_FK = idPlato";
        }

        //Para editar:
        function editar()
        {
            return "UPDATE plato
                    SET nombre = '". $this -> nombre . "', descripcion = '". $this -> descripcion . "', foto = '". $this -> foto . "', precio = '". $this -> precio . "', descripcion_receta = '". $this -> descripcion_receta . "', idCategoria_FK = '". $this -> idCategoria . "', idRegion_FK = '". $this -> idRegion . "', idComplejidad_FK = '". $this -> idComplejidad . "'
                    WHERE idPlato = '" . $this -> idPlato . "'";
        }

        //Para consultar el plato mas vendido:
        function platoMasVendido()
        {
            return "SELECT p.nombre, count(b.idPlato_FK) AS total
                    FROM plato p JOIN bandeja b on (p.idPlato = b.idPlato_FK)
                    group by p.nombre
                    order by total desc";
        }
    }
?>
