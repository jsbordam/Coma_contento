<?php
    class IngredienteDAO
    {
        private $idIngrediente;
        private $nombre;
        private $cantidad;
        private $unidad_medida;
        
        
        //Constructor:
        
        function IngredienteDAO ($pIdIngrediente="", $pNombre="", $pCantidad="", $pUnidad_medida="")
        {
            $this -> idIngrediente = $pIdIngrediente;
            $this -> nombre = $pNombre;
            $this -> cantidad = $pCantidad;
            $this -> unidad_medida = $pUnidad_medida;
        }
        
        //Metodos para las consultas en la BD:
        
        //Para crear:
        function crear()
        {
            return "INSERT into ingrediente (idIngrediente, nombre, cantidad, unidad_medida)
                    VALUES ('" . $this -> idIngrediente . "', '" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> unidad_medida . "')";
        }

        //Para consultar el ultimo id que se acaba de ingresar:
        function ultimoId()
        {
            return "SELECT MAX(idIngrediente)
                    FROM ingrediente";
        }

        //Para consultar todos los ingredientes de un plato:
        function consultar ($idPlato)
        {
            return "SELECT idIngrediente, nombre, cantidad, unidad_medida
                    FROM ingrediente, ingrediente_plato
                    WHERE idPlato_FK = '" . $idPlato . "' AND idIngrediente = idIngrediente_FK";
        }

        //Para consultar todos los ingredientes de una orden:
        function consultarIngre ($orden)
        {
            return "SELECT idIngrediente, nombre, cantidad, unidad_medida
                    FROM ingrediente
                    WHERE idIngrediente IN (SELECT idIngrediente_FK
                                            FROM ingrediente_plato
                                            WHERE idPlato_FK IN (SELECT idPlato_FK
                                                                 FROM bandeja
                                                                 WHERE idOrden_FK = '" . $orden . "'));";
        }

        
        //Para consultar todos los ingredientes de una plato:
        function ingrePlato()
        {
            return "SELECT p.nombre, count(i.idIngrediente_FK) AS total
                    FROM plato p JOIN ingrediente_plato i on (p.idPlato = i.idPlato_FK)
                    group by p.nombre
                    order by total desc";
        }
    }
?>
