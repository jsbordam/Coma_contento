<?php 
    class ProcesoDAO
    {
        private $idProceso;
        private $idOrden;
        private $idUsuario;
        private $estado;
        private $fecha_hora;
        private $obs;
        
        
        //Constructor:
        
        function ProcesoDAO ($pIdProceso="", $pIdOrden="", $pIdUsuario="", $pEstado="", $pFecha_hora="", $pObs="")
        {
            $this -> idProceso = $pIdProceso;
            $this -> idOrden = $pIdOrden;
            $this -> idUsuario = $pIdUsuario;
            $this -> estado = $pEstado;
            $this -> fecha_hora = $pFecha_hora;
            $this -> obs = $pObs;
        }


        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into proceso (idOrden_FK, idUsuario_FK, estado, fecha_hora, observaciones)
                    VALUES ('" . $this -> idOrden . "', '" . $this -> idUsuario . "', '" . $this -> estado . "', '" . $this -> fecha_hora . "', '" . $this -> obs . "')";
        }       
        
        //Para consultar un proceso que tenga estado (especifico):
        function consultar($estado)
        {
            return "SELECT idProceso, idOrden_FK, idUsuario_FK, estado, fecha_hora, observaciones
                    FROM proceso
                    WHERE idOrden_FK = '" . $this -> idOrden . "' && estado = " . $estado . "";
        }
        
        

    }
?>