<?php

    class OrdenDAO
    {
        private $idOrden;
        private $fecha_hora;
        private $valor_total;
        private $estado;
        private $idUsuario;
        private $obs;
        
        //Constructor:
        
        function OrdenDAO ($pIdOrden="", $pFecha_hora="", $pValor_total="", $pEstado="", $pIdUsuario="", $pIdObs)
        {
            $this -> idOrden = $pIdOrden;
            $this -> fecha_hora = $pFecha_hora;
            $this -> valor_total = $pValor_total;
            $this -> estado = $pEstado;
            $this -> idUsuario = $pIdUsuario;
            $this -> obs = $pIdObs;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para crear:
        function crear()
        {
            return "INSERT into orden (fecha_hora, valor_total, estado, idUsuario_FK)
                    VALUES ('" . $this -> fecha_hora . "', '" . $this -> valor_total . "', '" . $this -> estado . "', '" . $this -> idUsuario . "')";
        }       
        
        //Para consultar un pedido que tenga estado (especifico):
        function consultar($estado)
        {
            return "SELECT idOrden, fecha_hora, valor_total, estado
                    FROM orden
                    WHERE idUsuario_FK = '" . $this -> idUsuario . "' && estado = " . $estado . "";
        }
        
        //Para consultar un pedido:
        function consultarUno()
        {
            return "SELECT idOrden, fecha_hora, valor_total, estado, observaciones
                    FROM orden
                    WHERE idOrden = '" . $this -> idOrden . "'";
        }
        
        
        //Para consultar todos los pedidos de un usuario:
        function consultarTodos()
        {
            return "SELECT idOrden, fecha_hora, valor_total, estado
                    FROM orden
                    WHERE idUsuario_FK = '" . $this -> idUsuario . "' && estado != '0'";
        }
        
        //Para consultar todos los pedidos que existen:
        function consultarTodos2()
        {
            return "SELECT idOrden, fecha_hora, valor_total, estado, idUsuario_FK, observaciones
                    FROM orden";
        }
        
        //Para editar estado a 2 y valor total:
        function editar()
        {
            return "UPDATE orden
                    SET estado = '2', fecha_hora = '" . $this -> fecha_hora . "', valor_total = '" . $this -> valor_total . "'
                    WHERE idOrden = '" . $this -> idOrden . "'";
        }

        //Para editar estado de la orden en cada proceso:
        function editar2($estado)
        {
            return "UPDATE orden
                    SET estado = '" . $estado . "'
                    WHERE idOrden = '" . $this -> idOrden . "'";
        }

        //Para actualizar observaciones:
        function editar3($obs)
        {
            return "UPDATE orden
                    SET observaciones = '" . $obs . "'
                    WHERE idOrden = '" . $this -> idOrden . "'";
        }
        
        //Para eliminar un pedido:
        function eliminar()
        {
            return "DELETE FROM orden
                    WHERE idOrden = '" . $this -> idOrden . "'";
        }

        //Para ventas por mes de un año:
        function consultarVentasPorMes($año)
        {
            return "SELECT MONTHNAME(fecha_hora) AS Mes,
                    SUM(DISTINCT valor_total) AS Total, SUM(DISTINCT cantidad_und) AS Unidades
                    FROM orden, bandeja
                    WHERE idOrden_FK = idOrden && YEAR(fecha_hora) = '" . $año . "'
                    GROUP BY Mes
                    ORDER BY Mes ASC";
        }

        //Para consultar el ultimo id que se acaba de ingresar:
        function ultimoId()
        {
            return "SELECT MAX(idOrden)
                    FROM orden
                    WHERE idUsuario_FK = '" . $this -> idUsuario . "'";
        }

        //Para consultar todos los pedidos que existen:
        function consultarTodos3($estado, $id)
        {
            return "SELECT idOrden, orden.estado, orden.observaciones
                    FROM orden, proceso
                    WHERE idOrden = proceso.idOrden_FK && proceso.estado = '" . $estado . "' && proceso.idUsuario_FK = '" . $id . "'";
        }
    }

?>
