<?php

    class BandejaDAO
    {
        private $idOrden;
        private $idPlato;
        private $cantidad_und;
        
        
        //Constructor:
        
        function BandejaDAO ($pIdOrden="", $pIdPlato="", $pCantidad_und="")
        {
            $this -> idOrden = $pIdOrden;
            $this -> idPlato = $pIdPlato;
            $this -> cantidad_und = $pCantidad_und;          
        }
        
        //Metodos para las consultas en la BD:
        
        //Para crear:
        function crear()
        {
            return "INSERT into bandeja (idOrden_FK, idPlato_FK, cantidad_und)
                    VALUES ('" . $this -> idOrden . "', '" . $this -> idPlato . "', '" . $this -> cantidad_und . "')";
        }
        
        //Para consultar un pedido que tenga productos repetidos:
        function consultar()
        {
            return "SELECT idOrden_FK, idPlato_FK, cantidad_und
                    FROM bandeja
                    WHERE idPlato_FK = '" . $this -> idPlato . "' && idOrden_FK = '" . $this -> idOrden . "'";
        }       
        
        //Para consultar todos:
        function consultarTodos()
        {
            return "SELECT idOrden_FK, idPlato_FK, cantidad_und
                    FROM bandeja
                    WHERE idOrden_FK = '" . $this -> idOrden . "'";
        }
        
        //Para editar:
        function editar()
        {
            return "UPDATE bandeja
                    SET cantidad_und = '". $this -> cantidad_und . "'
                    WHERE idPlato_FK = '" . $this -> idPlato . "' && idOrden_FK = '" . $this -> idOrden . "'";
        }
        
        //Para sumar los productos que hay en el carrito:
        function sumarProductos()
        {
            return "SELECT SUM(cantidad_und)
                    FROM bandeja
                    WHERE idOrden_FK = '" . $this -> idOrden . "'";
        }
        
        
        //Para eliminar productos del carrito:
        function eliminar()
        {
            return "DELETE FROM bandeja                
                    WHERE idPlato_FK = '" . $this -> idPlato . "'";
        }
        
        //Para eliminar todos los productos de un pedido:
        function eliminar2()
        {
            return "DELETE FROM bandeja
                    WHERE idOrden_FK = '" . $this -> idOrden . "'";
        }
    }

?>
