<?php
    class RegionDAO
    {
        private $idRegion;
        private $nombre;
        private $idUsuario;
        
        
        //Constructor:
        
        function RegionDAO ($pIdRegion="", $pNombre="", $pIdUsuario="")
        {
            $this -> idRegion = $pIdRegion;
            $this -> nombre = $pNombre;
            $this -> idUsuario = $pIdUsuario;
        }
        
        //Metodos para las consultas en la BD:
        
        
        //Para consultar una:
        function consultar()
        {
            return "SELECT idRegion, nombre, idUsuario_FK
                    FROM region
                    WHERE idRegion = '" . $this -> idRegion . "'";
        }     


        //Para editar:
        function editar()
        {
            return "UPDATE region
                    SET idUsuario_FK = '". $this -> idUsuario . "'
                    WHERE idRegion = '" . $this -> idRegion . "'";
        }
    }
?>
