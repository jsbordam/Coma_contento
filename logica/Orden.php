<?php
    //Para importar las clases que necesito usar:
    require "persistencia/OrdenDAO.php";
    
    class Orden
    {
        private $idOrden;
        private $fecha_hora;
        private $valor_total;
        private $estado;
        private $idUsuario;
        private $obs;
        
        private $conexion;
        private $ordenDAO;
        
        
        //Constructor:
        
        function Orden ($pIdOrden="", $pFecha_hora="", $pValor_total="", $pEstado="", $pIdUsuario="", $pIdObs="")
        {
            $this -> idOrden = $pIdOrden;
            $this -> fecha_hora = $pFecha_hora;
            $this -> valor_total = $pValor_total;
            $this -> estado = $pEstado;
            $this -> idUsuario = $pIdUsuario;
            $this -> obs = $pIdObs;
            $this -> conexion = new Conexion();
            $this -> ordenDAO = new OrdenDAO ($pIdOrden, $pFecha_hora, $pValor_total, $pEstado, $pIdUsuario, $pIdObs);
        }
        
        
        //Metodos GET:
        
        public function getIdOrden()
        {
            return $this->idOrden;
        }
        
        public function getFecha_hora()
        {
            return $this->fecha_hora;
        }
        
        public function getValor_total ()
        {
            return $this->valor_total;
        }
            
        public function getEstado ()
        {
            return $this->estado;
        }
            
        public function getIdUsuario ()
        {
            return $this->idUsuario;
        }

        public function getObs ()
        {
            return $this->obs;
        }
              
        
                    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear una orden:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> crear();
            $this -> conexion -> ejecutar($this -> ordenDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        
        //Metodo para consultar una orden que tenga un estado especifico:
        function consultar($estado)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> consultar($estado);
            $this -> conexion -> ejecutar($this -> ordenDAO -> consultar($estado));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            if ($this -> conexion -> numFilas() == 0)
            {
                //Quiere decir que no se encontro ningun resultado, 
                //entonces es el primer pedido:
                $this -> estado = 0;
            }
            else
            {
                $this -> idOrden = $resultado[0];
                $this -> fecha_hora = $resultado[1];
                $this -> valor_total = $resultado[2];
                $this -> estado = $resultado[3];
            }         
        }  
        
        //Metodo para consultar un pedido especifico:
        function consultarUno()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> consultarUno();
            $this -> conexion -> ejecutar($this -> ordenDAO -> consultarUno());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Si el pedido existe lo trae:
            if ($this -> conexion -> numFilas() == 1)
            {
                $resultado = $this -> conexion -> extraer();
                $this -> idOrden = $resultado[0];
                $this -> fecha_hora = $resultado[1];
                $this -> valor_total = $resultado[2];
                $this -> estado = $resultado[3];
                $this -> obs = $resultado[4];
                return true;
            } 
            else 
            {
                return false;
            }
        }
        
        
        //Metodo para consultar todos los pedidos de un usuario:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> ordenDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $pedidos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($pedidos, new Orden ($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
            }
            return $pedidos;
        }
        
        //Metodo para consultar todos los pedidos que existen:
        function consultarTodos2()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> consultarTodos2();
            $this -> conexion -> ejecutar($this -> ordenDAO -> consultarTodos2());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $pedidos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($pedidos, new Orden ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
            }
            return $pedidos;
        }
        
        //Metodo para editar el estado de una orden, cuando es solicitado:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> editar();
            $this -> conexion -> ejecutar($this -> ordenDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para editar el estado de una orden:
        function editar2($estado)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> editar2($estado);
            $this -> conexion -> ejecutar($this -> ordenDAO -> editar2($estado));
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        //Metodo para actualizar las observaciones de una orden:
        function editar3($obs)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> editar3($obs);
            $this -> conexion -> ejecutar($this -> ordenDAO -> editar3($obs));
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para eliminar una orden:
        function eliminar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> eliminar();
            $this -> conexion -> ejecutar($this -> ordenDAO -> eliminar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para consultar ventas en dinero por mes y año
        function consultarVentasPorMes($año)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> ordenDAO -> consultarVentasPorMes($año);
            $this -> conexion -> ejecutar($this -> ordenDAO -> consultarVentasPorMes($año));
            $this -> conexion -> cerrar();
            $resultados = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($resultados, array($resultado[0], $resultado[1], $resultado[2]));
            }
            return $resultados;
        }


        //Metodo para consultar la ultima orden realizada por un usuario:
        function ultimoId()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> ultimoId();
            $this -> conexion -> ejecutar($this -> ordenDAO -> ultimoId());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> idOrden = $resultado[0];
        }

        //Metodo para consultar las ordenes que fueron recibidas por un chef:
        function consultarTodos3($estado, $id)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ordenDAO -> consultarTodos3($estado, $id);
            $this -> conexion -> ejecutar($this -> ordenDAO -> consultarTodos3($estado, $id));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $pedidos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($pedidos, new Orden ($resultado[0], "", "", $resultado[1], "", $resultado[2]));
            }
            return $pedidos;
        }
    }
?>