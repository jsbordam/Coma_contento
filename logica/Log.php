<?php
    //Para importar las clases que necesito usar:
    require "persistencia/LogDAO.php";

    class Log
    {
        private $idLog;
        private $accion;
        private $datos;
        private $fecha;
        private $hora;
        private $ip;
        private $so;
        private $idUsuario;

        
        private $conexion;
        private $logDAO;
        
        
    
        //Constructor:
        function Log ($pIdLog="", $pAccion="", $pDatos="", $pFecha="", $pHora="", $pIp="", $pSo="", $pIdUsuario="")
        {
            $this -> idLog = $pIdLog;
            $this -> accion = $pAccion;
            $this -> datos = $pDatos;
            $this -> fecha = $pFecha;
            $this -> hora = $pHora;
            $this -> ip = $pIp;
            $this -> so = $pSo;
            $this -> idUsuario = $pIdUsuario;
            $this -> conexion = new Conexion();
            $this -> logDAO = new LogDAO ($pIdLog, $pAccion, $pDatos, $pFecha, $pHora, $pIp, $pSo, $pIdUsuario);
        }
        
        
        //Metodos GET:
        
        public function getIdLog()
        {
            return $this->idLog;
        }
        
        public function getAccion()
        {
            return $this->accion;
        }
        
        public function getDatos()
        {
            return $this->datos;
        }

        public function getFecha()
        {
            return $this->fecha;
        }
        
        public function getHora()
        {
            return $this->hora;
        }
        
        public function getIp()
        {
            return $this->ip;
        }
        
        public function getSo()
        {
            return $this->so;
        }

        public function getIdUsuario()
        {
            return $this->idUsuario;
        }

        public function getConexion()
        {
            return $this->conexion;
        }

        public function getLogDAO()
        {
            return $this->logusuarioDAO;
        }
        
        
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un log:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> logDAO -> crear();
            $this -> conexion -> ejecutar($this -> logDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        
        
        //Metodo para buscar filtro de LOG con AJAX:
        function buscar($filtro)
        {
            $this -> conexion -> abrir();
            //Para ver la consulta sql = echo $this -> logDAO -> buscar($filtro);
            $this -> conexion -> ejecutar($this -> logDAO -> buscar($filtro));
            $this -> conexion -> cerrar();
            $log = array();
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($log, new Log ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]));
            }
            return $log;
        }  
        
    }
?>