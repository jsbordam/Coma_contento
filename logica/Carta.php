<?php
    //Para importar las clases que necesito usar:
    require "persistencia/CartaDAO.php";
    
    class Carta
    {
        private $idCarta;
        private $nombre;
        private $vigencia;
        private $estado;
        
        private $conexion;
        private $cartaDAO;
        
        
        //Constructor:
        
        function Carta ($pIdCarta="", $pNombre="", $pVigencia="", $pEstado="")
        {
            $this -> idCarta = $pIdCarta;
            $this -> nombre = $pNombre;
            $this -> vigencia = $pVigencia;
            $this -> estado = $pEstado;
            $this -> conexion = new Conexion();
            $this -> cartaDAO = new CartaDAO ($pIdCarta, $pNombre, $pVigencia, $pEstado);
        }
        
        
        //Metodos GET:
        
        public function getIdCarta()
        {
            return $this->idCarta;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getVigencia()
        {
            return $this->vigencia;
        }

        public function getEstado()
        {
            return $this->estado;
        }

    

    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear una carta:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> cartaDAO -> crear();
            $this -> conexion -> ejecutar($this -> cartaDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        
        //Metodo para consultar si existe una carta con un nombre especifico
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> cartaDAO -> consultar();
            $this -> conexion -> ejecutar($this -> cartaDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion  
            
            //Si no existe una carta con ese nombre entonces el resultado es 0:
            if ($this -> conexion -> numFilas() > 0)
            {
                $resultado = $this -> conexion -> extraer();
                $this -> idCarta = $resultado[0];
                $this -> nombre = $resultado[1];
                $this -> vigencia = $resultado[2];
                $this -> estado = $resultado[3];
                return true;
            }
            else
            {
                return false;
            }
        }
        
        //Metodo para consultar todos las cartas:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> cartaDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> cartaDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $cartas = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push ($cartas, new Carta ($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
            }
            return $cartas;
        }

        //Metodo para editar una carta:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> cartaDAO -> editar();
            $this -> conexion -> ejecutar($this -> cartaDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para cambiar la carta activa:
        function activa()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> cartaDAO -> activa();
            $this -> conexion -> ejecutar($this -> cartaDAO -> activa());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para consultar cantidad de platos en cada carta:
        function platosEnCarta()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> cartaDAO -> platosEnCarta();
            $this -> conexion -> ejecutar($this -> cartaDAO -> platosEnCarta());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            $resultados = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($resultados, array($resultado[0],$resultado[1]));
            }
            return $resultados;
        }
}
?>