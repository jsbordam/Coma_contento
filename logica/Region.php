<?php
    //Para importar las clases que necesito usar:
    require "persistencia/RegionDAO.php";
    
    class Region
    {
        private $idRegion;
        private $nombre;
        private $idUsuario;
        
        private $conexion;
        private $regionDAO;
        
        
        //Constructor:
        
        function Region ($pIdRegion="", $pNombre="", $pIdUsuario="")
        {
            $this -> idRegion = $pIdRegion;
            $this -> nombre = $pNombre;
            $this -> idUsuario = $pIdUsuario;
            $this -> conexion = new Conexion();
            $this -> regionDAO = new RegionDAO ($pIdRegion, $pNombre, $pIdUsuario);
        }
        
        
        //Metodos GET:
        
        public function getIdRegion()
        {
            return $this->idRegion;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getIdUsuario()
        {
            return $this->idUsuario;
        }

    

    
        //Metodos para conectar con las consultas de la BD: 
      
        
        //Metodo para consultar si existe una carta con un nombre especifico
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> regionDAO -> consultar();
            $this -> conexion -> ejecutar($this -> regionDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion  
            $resultado = $this -> conexion -> extraer();
            $this -> idRegion = $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> idUsuario = $resultado[2];         
        }
        

        //Metodo para editar una carta:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> regionDAO -> editar();
            $this -> conexion -> ejecutar($this -> regionDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
}
?>