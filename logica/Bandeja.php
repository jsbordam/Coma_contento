<?php
    //Para importar las clases que necesito usar:
    require "persistencia/BandejaDAO.php";
    
    class Bandeja
    {
        private $idOrden;
        private $idPlato;
        private $cantidad_und;
        
        private $conexion;
        private $bandejaDAO;
        
        
        //Constructor:

        function Bandeja ($pIdOrden="", $pIdPlato="", $pCantidad_und="")
        {
            $this -> idOrden = $pIdOrden;
            $this -> idPlato = $pIdPlato;
            $this -> cantidad_und = $pCantidad_und;       
            $this -> conexion = new Conexion();
            $this -> bandejaDAO = new BandejaDAO ($pIdOrden, $pIdPlato, $pCantidad_und);   
        }
        
        
        //Metodos GET:
        
        public function getIdOrden()
        {
            return $this->idOrden;
        }
        
        public function getIdPlato()
        {
            return $this->idPlato;
        }
        
        public function getCantidad_und()
        {
            return $this->cantidad_und;
        }

    

    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un carrito:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> crear();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        
        //Metodo para consultar un pedido que tenga el mismo idPedido e idProducto
        //Para cuando se quiera agregar un mismo producto en un mismo pedido:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> consultar();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion            
            
            //Para saber si se quiere añadir un mismo producto a un mismo pedido:
            if ($this -> conexion -> numFilas() == 1)
            {
                $resultado = $this -> conexion -> extraer();
                $this -> idOrden = $resultado[0];
                $this -> idPlato = $resultado[1];
                $this -> cantidad_und = $resultado[2]; 
                return true; 
            }
            else //No encontro ningun registro con el mismo idPedido e idProducto:
            {
                return false;
            }
        }  
       
        
        //Metodo para consultar todos los datos de la bandeja:
        function consultarTodos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> consultarTodos();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> consultarTodos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $carrito = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($carrito, new Bandeja ($resultado[0], $resultado[1], $resultado[2]));
            }
            return $carrito;
        }
        
        //Metodo para editar la cantidad de unidades de producto de un carrito:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> editar();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        //Metodo para consultar la cantidad de productos dentro del carrito:
        function sumarProductos()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> sumarProductos();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> sumarProductos());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> cantidad_und = $resultado[0]; 
            
            if ($this -> cantidad_und == NULL) //Si no hay resultados
            {
                $this -> cantidad_und = 0;
            }                                   
        }  
        
        //Metodo para eliminar un producto de carrito:
        function eliminar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> eliminar();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> eliminar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        //Metodo para eliminar todos los productos que hayan en un pedido:
        function eliminar2()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> bandejaDAO -> eliminar2();
            $this -> conexion -> ejecutar($this -> bandejaDAO -> eliminar2());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
    }
?>