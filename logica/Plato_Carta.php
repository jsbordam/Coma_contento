<?php
    //Para importar las clases que necesito usar:
    require "persistencia/Plato_CartaDAO.php";
    
    class Plato_Carta
    {
        private $idPlato;
        private $idCarta;
        
        private $conexion;
        private $platocDAO;
        
        
        //Constructor:
        
        function Plato_Carta ($pIdPlato="", $pIdCarta="")
        {
            $this -> idPlato = $pIdPlato;
            $this -> idCarta = $pIdCarta;
            $this -> conexion = new Conexion();
            $this -> platocDAO = new Plato_CartaDAO ($pIdPlato, $pIdCarta);
        }
        
        
        //Metodos GET:
        
        public function getIdPlato()
        {
            return $this->idPlato;
        }
        
        public function getIdCarta()
        {
            return $this->idCarta;
        }

    
    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear la relación:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platocDAO -> crear();
            $this -> conexion -> ejecutar($this -> platocDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para editar una carta:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platocDAO -> editar();
            $this -> conexion -> ejecutar($this -> platocDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
    }
?>