<?php
    //Para importar las clases que necesito usar:
    require "persistencia/Ingrediente_PlatoDAO.php";
    
    class Ingrediente_Plato
    {
        private $idIngrediente;
        private $idPlato;
        
        private $conexion;
        private $ingrepDAO;
        
        
        //Constructor:
        
        function Ingrediente_Plato ($pIdIngrediente="", $pIdPlato="")
        {
            $this -> idIngrediente = $pIdIngrediente;
            $this -> idPlato = $pIdPlato;
            $this -> conexion = new Conexion();
            $this -> ingrepDAO = new Ingrediente_PlatoDAO ($pIdIngrediente, $pIdPlato);
        }
        
        
        //Metodos GET:
        
        public function getIdIngrediente()
        {
            return $this->idIngrediente;
        }
        
        public function getIdPlato()
        {
            return $this->idPlato;
        }

    

    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear la relación:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ingrepDAO -> crear();
            $this -> conexion -> ejecutar($this -> ingrepDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
    }
?>