<?php
    //Para importar las clases que necesito usar:
    require "persistencia/ProcesoDAO.php";
    
    class Proceso
    {
        private $idProceso;
        private $idOrden;
        private $idUsuario;
        private $estado;
        private $fecha_hora;
        private $obs;
        
        private $conexion;
        private $procesoDAO;
        
        
        //Constructor:
        
        function Proceso ($pIdProceso="", $pIdOrden="", $pIdUsuario="", $pEstado="", $pFecha_hora="", $pObs="")
        {
            $this -> idProceso = $pIdProceso;
            $this -> idOrden = $pIdOrden;
            $this -> idUsuario = $pIdUsuario;
            $this -> estado = $pEstado;
            $this -> fecha_hora = $pFecha_hora;
            $this -> obs = $pObs;
            $this -> conexion = new Conexion();
            $this -> procesoDAO = new ProcesoDAO ($pIdProceso, $pIdOrden, $pIdUsuario, $pEstado, $pFecha_hora, $pObs);
        }
        
        
        //Metodos GET:

        public function getIdProceso()
        {
            return $this->idProceso;
        }
        
        public function getIdOrden()
        {
            return $this->idOrden;
        }

        public function getIdUsuario ()
        {
            return $this->idUsuario;
        }

        public function getEstado ()
        {
            return $this->estado;
        }
        
        public function getFecha_hora()
        {
            return $this->fecha_hora;
        }

        public function getObs()
        {
            return $this->obs;
        }


        //Para recoger algunos datos del usuario para el LOG:

        public $ip;
        public $so;
        function datosLog()
        {  
            //Capturamos su Ip:
            $m = "";
            $externalContent = file_get_contents('http://checkip.dyndns.com/');
            preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
            
            $this->ip = $m[1];
            
            
            //Capturamos su Sistema Operativo (SO):
            $identificador = getenv("HTTP_USER_AGENT");
            
            
            function getPlatform($identificador)
            {
                $plataformas = array(
                    'Windows 10' => 'Windows NT 10.0+',
                    'Windows 8.1' => 'Windows NT 6.3+',
                    'Windows 8' => 'Windows NT 6.2+',
                    'Windows 7' => 'Windows NT 6.1+',
                    'Windows Vista' => 'Windows NT 6.0+',
                    'Windows XP' => 'Windows NT 5.1+',
                    'Windows 2003' => 'Windows NT 5.2+',
                    'Windows' => 'Windows otros',
                    'iPhone' => 'iPhone',
                    'iPad' => 'iPad',
                    'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
                    'Mac otros' => 'Macintosh',
                    'Android' => 'Android',
                    'BlackBerry' => 'BlackBerry',
                    'Linux' => 'Linux',
                );
                foreach ($plataformas as $plataforma=>$pattern)
                {
                    if (preg_match('/(?i)'.$pattern.'/', $identificador))
                        return $plataforma;
                }
                return 'Otras';
            }
            
            $this->so = getPlatform($identificador);
        }
        
        
        
                    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para registrar el proceso:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> procesoDAO -> crear();
            $this -> conexion -> ejecutar($this -> procesoDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion

            //Se genera reporte de LOG:
            if ($this->estado == 3)
            {
                $est = "Atendida por chef";
            }
            else if ($this->estado == 4)
            {
                $est = "Preparada por chef";
            }
            else
            {
                $est = "Entregada por mesero";
            }



            $this -> datosLog();
            $datos = "Codigo: " . $this -> idProceso .
                    "   Id Orden: " . $this -> idOrden .
                    "   Id Usuario que gestiono la orden: " . $this -> idUsuario .
                    "   Estado: " . $est .
                    "   Fecha - Hora: " . $this -> fecha_hora .
                    "   Observaciones: " . $this -> obs;
            
            $log = new Log ("", "Proceso Orden", $datos, date("Y/m/d"), date("G:i:s"), $this->ip, $this->so, $_SESSION["id"]);
            $log -> crear();
        }
        
        
        //Metodo para consultar un proceso de una orden con estado especifico:
        function consultar($estado)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> procesoDAO -> consultar($estado);
            $this -> conexion -> ejecutar($this -> procesoDAO -> consultar($estado));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Si el proceso existe lo trae:
            if ($this -> conexion -> numFilas() == 1)
            {
                $resultado = $this -> conexion -> extraer();
                $this -> idProceso = $resultado[0];
                $this -> idOrden = $resultado[1];
                $this -> idUsuario = $resultado[2];
                $this -> estado = $resultado[3];
                $this -> fecha_hora = $resultado[4];
                $this -> obs = $resultado[5];
                return true;
            } 
            else 
            {
                return false;
            }
        }
        
    }
?>