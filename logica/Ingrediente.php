<?php
    //Para importar las clases que necesito usar:
    require "persistencia/IngredienteDAO.php";
    
    class Ingrediente
    {
        private $idIngrediente;
        private $nombre;
        private $cantidad;
        private $unidad_medida;
        
        private $conexion;
        private $ingredienteDAO;
        
        
        //Constructor:
        
        function Ingrediente ($pIdIngrediente="", $pNombre="", $pCantidad="", $pUnidad_medida="")
        {
            $this -> idIngrediente = $pIdIngrediente;
            $this -> nombre = $pNombre;
            $this -> cantidad = $pCantidad;
            $this -> unidad_medida = $pUnidad_medida;
            $this -> conexion = new Conexion();
            $this -> ingredienteDAO = new IngredienteDAO ($pIdIngrediente, $pNombre, $pCantidad, $pUnidad_medida);
        }
        
        
        //Metodos GET:
        
        public function getIdIngrediente()
        {
            return $this->idIngrediente;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getCantidad()
        {
            return $this->cantidad;
        }

        public function getUnidad_medida()
        {
            return $this->unidad_medida;
        }

    

    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear una ingre:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ingredienteDAO -> crear();
            $this -> conexion -> ejecutar($this -> ingredienteDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para ultimo id insertado:
        function ultimoId()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ingredienteDAO -> ultimoId();
            $this -> conexion -> ejecutar($this -> ingredienteDAO -> ultimoId());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> idIngrediente = $resultado[0];
        }

        //Metodo para consultar los ingredientes de un plato:
        function consultar($idPlato)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ingredienteDAO -> consultar($idPlato);
            $this -> conexion -> ejecutar($this -> ingredienteDAO -> consultar($idPlato));
            $this -> conexion -> cerrar(); //Se cierra la conexion

            $ingredientes = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($ingredientes, new Ingrediente ($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
            }
            return $ingredientes;
        }

        //Metodo para consultar los ingredientes que fueron utilizados en los platos de una orden:
        function consultarIngre($orden)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ingredienteDAO -> consultarIngre($orden);
            $this -> conexion -> ejecutar($this -> ingredienteDAO -> consultarIngre($orden));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $ingredientes = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($ingredientes, new Ingrediente ($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
            }
            return $ingredientes;
        }

        //Metodo para consultar los ingredientes de un plato:
        function ingrePlato()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> ingredienteDAO -> ingrePlato();
            $this -> conexion -> ejecutar($this -> ingredienteDAO -> ingrePlato());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            $resultados = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($resultados, array($resultado[0],$resultado[1]));
            }
            return $resultados;
        }
       
    }
?>