<?php
    //Para importar las clases que necesito usar:
    require "persistencia/PlatoDAO.php";
    
    class Plato
    {
        private $idPlato;
        private $nombre;
        private $descripcion;
        private $foto;
        private $precio;
        private $descripcion_receta;
        private $idCategoria;
        private $idRegion;
        private $idComplejidad;

        
        private $conexion;
        private $platoDAO;
        
        
        //Constructor:
        
        function Plato ($pIdPlato="", $pNombre="", $pDescripcion="", $pFoto="", $pPrecio="", $pDescripcion_receta="", $pIdCategoria="", $pIdRegion="", $pIdComplejidad="")
        {
            $this -> idPlato = $pIdPlato;
            $this -> nombre = $pNombre;
            $this -> descripcion = $pDescripcion;
            $this -> foto = $pFoto;
            $this -> precio = $pPrecio;
            $this -> descripcion_receta = $pDescripcion_receta;
            $this -> idComplejidad = $pIdComplejidad;
            $this -> idCategoria = $pIdCategoria;
            $this -> idRegion = $pIdRegion;
            $this -> conexion = new Conexion();
            $this -> platoDAO = new PlatoDAO ($pIdPlato, $pNombre, $pDescripcion, $pFoto, $pPrecio, $pDescripcion_receta, $pIdCategoria, $pIdRegion, $pIdComplejidad);
        }
        
        
        //Metodos GET:
        
        public function getIdPlato()
        {
            return $this->idPlato;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }
        
        public function getDescripcion()
        {
            return $this->descripcion;
        }

        public function getFoto()
        {
            return $this->foto;
        }
        
        public function getPrecio()
        {
            return $this->precio;
        }
        
        public function getDescripcion_receta()
        {
            return $this->descripcion_receta;
        }

        public function getIdComplejidad()
        {
            return $this->idComplejidad;
        }
        
        public function getIdCategoria()
        {
            return $this->idCategoria;
        }
        
        public function getIdRegion()
        {
            return $this->idRegion;
        }



    
        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un plato:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> crear();
            $this -> conexion -> ejecutar($this -> platoDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }
        
        
        //Metodo para consultar un plato:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> consultar();
            $this -> conexion -> ejecutar($this -> platoDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> idPlato = $resultado[0];
            $this -> nombre = $resultado[1];
            $this -> descripcion = $resultado[2]; 
            $this -> foto = $resultado[3];
            $this -> precio = $resultado[4];
            $this -> descripcion_receta = $resultado[5];
            $this -> idCategoria = $resultado[6];
            $this -> idRegion = $resultado[7];
            $this -> idComplejidad = $resultado[8];
        }

        //Metodo para consultar todos los platos:
        function consultarTodos($idCar)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> consultarTodos($idCar);
            $this -> conexion -> ejecutar($this -> platoDAO -> consultarTodos($idCar));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $platos = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push ($platos, new Plato ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8]));
            }
            return $platos;
        }

        //Metodo para ultimo id insertado:
        function ultimoId()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> ultimoId();
            $this -> conexion -> ejecutar($this -> platoDAO -> ultimoId());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $resultado = $this -> conexion -> extraer();
            $this -> idPlato = $resultado[0];
        }

        //Metodo para contar los platos de una region especifica:
        function contarPla($idCarta)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> contarPla($idCarta);
            $this -> conexion -> ejecutar($this -> platoDAO -> contarPla($idCarta));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            //Retorno el numero de resultados que seria el numero de platos que hay 
            //en esa categoria:
            return $this -> conexion -> numFilas();    
        }

        //Metodo para editar una plato:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> editar();
            $this -> conexion -> ejecutar($this -> platoDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }

        //Metodo para consultar el plato mas vendido:
        function platoMasVendido()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> platoDAO -> platoMasVendido();
            $this -> conexion -> ejecutar($this -> platoDAO -> platoMasVendido());
            $this -> conexion -> cerrar(); //Se cierra la conexion
            
            $resultados = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($resultados, array($resultado[0],$resultado[1]));
            }
            return $resultados;
        }
    }
?>