<!-- Modal body -->
<div class="modal-body">
    <div class="col-md-12">
        <h1 align="center">
            <font face="cursive" size="6" color="white">
                Iniciar Sesión
            </font>
        </h1>
    </div>
    <hr class="divider hr-xl-center-0 bg-mantis offset-top-30">
    <br>
    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/autenticar.php")?> method="post">
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="email" name="correo" class="form-control" placeholder="Correo" required="required">
        </div>
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" name="clave" class="form-control" placeholder="Clave" required="required">
            <br>
        </div>
        <div class="input-group form-group">
            <font color="white"><a href="#">Olvidaste tu clave?</a></font>
        </div>

        <div class="form-group">
            <input type="submit" id="entrar" value="Entrar" class="btn login_btn btn-block">
        </div>
    </form>

    <div class="d-flex justify-content-center links">
        <a href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/registrarse.php")?>">Registrarse</a>
    </div>

    <br>

    <div class="col-md-12">
        <div align="center">
            <a href="#">
                <font size="8" color="red"><i class="far fa-times-circle" data-dismiss="modal"></i></font>
            </a>
        </div>
    </div>
</div>