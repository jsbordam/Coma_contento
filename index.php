<?php 
    //Para recuperar la variable sesion cuando se recarga la pagina:
    session_start();

    //Para importar las clases que necesito usar:
    require "logica/Usuario.php";
    require "logica/Carta.php";
    require "logica/Plato.php";
    require "logica/Ingrediente.php";
    require "logica/Ingrediente_Plato.php";
    require "logica/Plato_Carta.php";
    require "logica/Region.php";
    require "logica/Orden.php";
    require "logica/Bandeja.php";
    require "logica/Proceso.php";
    require "logica/Log.php";
    require "persistencia/Conexion.php";
    
    //Zona horaria en colombia:
    date_default_timezone_set('America/Bogota'); 
    
    
    //Para cerrar la session:
    if (isset($_GET["session"]) && $_GET["session"] == 0)
    {
        $_SESSION["id"] = null; //Se vacia el objeto de session, para romper esa session.
    }
    
    //Este algoritmo es para que me muestre los errores
    //en tiempo de ejecución:
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    ////////////////////////////////////////////////////
    
    //Pid es una variable para redireccionar de una a otra pagina:
    $pid=NULL;
    if (isset($_GET["pid"]))
    {
        //En pid se guarda la ruta decodificada / desencriptada:
        $pid = base64_decode($_GET["pid"]);
    }
?>

<!-- RD Navbar Simple submenu-->
<!DOCTYPE html>
<html class="wow-animation" lang="en">

<head>
    <title>CC Restaurant</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport"
        content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="Intense Restaurant">
    <meta name="date" content="Apr 25">
    <!-- Icono de la pestaña: -->
    <link rel="icon" href="img/icono.png" type="image/x-icon">
    <!-- Lineas importantes: -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
        href="//fonts.googleapis.com/css?family=Dancing+Script:700%7CRaleway:300,400,600,700,900">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Esta linea es para implementar: Font Awesome (iconos) -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css">
    <link rel="stylesheet" href="css/style.css">
    <style>

    .ie-panel {
        display: none;
        background: #212121;
        padding: 10px 0;
        box-shadow: 3px 3px 5px 0 rgba(0, 0, 0, .3);
        clear: both;
        text-align: center;
        position: relative;
        z-index: 1;
    }

    html.ie-10 .ie-panel,
    html.lt-ie-10 .ie-panel {
        display: block;
    }
    </style>
    <!-- Estas lineas son para implementar: jQuery, Popper.js y Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js">
    </script>

     <!-- Template Specisifc Custom Javascript File -->
     <script src="js/custom.js"></script>

    <!-- Estas lineas son para implementar tooltip (Pequeñas etiquetas descriptivas de botones) -->
    <script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>

    <!-- Script para los graficos estadisticos de Google Chart:  -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Para desaparecer el contenido de un div despues de un tiempo: 4.000 = 4 segundos-->
    <script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $(".alerta").fadeOut(1500);
        },4000);
    });
    </script>
</head>



<body>
    <?php 
    	//Para validar si existe session y si es una pagina que no necesita session:
    	if (isset($pid))
    	{
    	    //Si existe m es porque se dirige para una seccion que 
    	    //requiere alguno de los 4 menus:
    	    if (isset($_GET["m"]))
    	    {
    	        //Si existe una sesion, verifica que menu necesita de los 3 roles:
    	        if (isset($_SESSION["id"]))
    	        {
    	            /*Se verifica que menu es el que necesita:
    	             - menu cliente
    	             - menu admin
    	             - menu chef
                     - menu mesero */
    	            
    	            if ($_SESSION["rol"] == "Administrador")
    	            {
    	                include "presentacion/administrador/menuAdministrador.php";
    	                include $pid;
    	            }
    	            else if ($_SESSION["rol"] == "Cliente")
    	            {
    	                include "presentacion/cliente/menuCliente.php";
    	                include $pid;
    	            }
                    else if ($_SESSION["rol"] == "Chef")
    	            {
    	                include "presentacion/chef/menuChef.php";
    	                include $pid;
    	            }
    	            else
    	            {
    	                include "presentacion/mesero/menuMesero.php";
    	                include $pid;
    	            }
    	        }
    	        else
    	        {
    	            //Quiere decir que no se ha iniciado sesion:
    	             	            
    	            //Sí existe verificar es porque se quiere añadir un plato a la bandeja,
    	            //entonces se debe reportar error porque no hay sesion activa:
    	            if (isset($_GET["verificar"]))
    	            {
    	                //Redirecciona a los mismos productos reportando un error:
    	                header ("location: index.php?pid=" . base64_encode("presentacion/platos/consultarPlatos.php") . "&m&error=1&idCar=" . $_GET["idCar"] . "&reg=" . $_GET["reg"] . "&idPla=" . $_GET["idPla"] . "");
    	            }
    	            else 
    	            {
    	                //Se dirige a una seccion del menu principal:
    	                include "presentacion/menuPrincipal/menu.php";
    	                include $pid;
    	            }
    	        }  	        	
    	    } 	  
    	    else
    	    {   
    	        //Todas las paginas que no necesitan ni encabezado, ni menu principal:
    	        include $pid;
    	    }
    	}
    	else
    	{   	   
    	    include "presentacion/menuPrincipal/inicio.php";
    	}
    ?>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>