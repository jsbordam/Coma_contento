<?php
    $chef = new Usuario($_SESSION["id"]);
    $chef -> consultar();
?>

<!-- Page-->
<div class="page text-center">
    <!-- Page Contents-->
    <main class="page-content">
        <!--Sección de encabezado de inicio: -->
        <section class="context-dark">
            <div class="parallax-container"
                data-parallax-img="https://www.floornature.es/media/photos/1/12833/04_cadena-asociados_carboncabron_ph-adrian_llaguno_full.jpg">
                <div class="parallax-content">
                    <div class="container wow fadeInUp">
                        <div class="section-110 section-cover row justify-content-sm-center align-items-sm-center">
                            <div class="col-lg-8">
                                <img src="<?php echo $chef -> getFoto(); ?>" width="250" height="230">
                                <h1 class="font-accent"><span
                                        class="big"><?php echo $chef -> getNombre() . " " . $chef -> getApellido(); ?></span>
                                </h1>
                                <h3><?php echo "Correo: " . $chef -> getCorreo(); ?></h3>
                                <h3><?php echo "Tienes un rol de: CHEF"; ?></h3>
                                <div class="group offset-top-50"><a class="btn btn-lg btn-primary" href="#"
                                        data-custom-scroll-to="reserve-now">Actualiza tus datos</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sección de bienvenida: -->
        <section id="portfolio">
            <section class="section-66 section-lg-110">
                <div class="container wow fadeInUp">
                    <div class="row justify-content-center grid-group-md text-xl-left">
                        <div class="col-md-8 col-xl-6">
                            <div class="inset-xl-right-20">
                                <h1><span class="d-block font-accent big">Bienvenido
                                        <span class="d-block offset-top-4 h3 text-light text-uppercase">
                                            <font color="black">
                                                <?php echo $chef -> getNombre()?>
                                            </font>
                                        </span>

                                    </span>
                                </h1>
                                <hr class="divider hr-xl-left-0 bg-mantis offset-top-30">
                                <p>
                                    En este espacio puedes gestionar todas las ordenes de los clientes
                                    que llegan para ser atendidas y posterioremnete preparadas.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8 col-xl-6">
                            <img src="https://p4.wallpaperbetter.com/wallpaper/367/884/987/interior-train-interior-design-restaurant-wallpaper-preview.jpg"
                                width="600px" height="400px">
                        </div>
                    </div>
                </div>
            </section>
        </section>

        <!--Seccion de Menu-->
        <section>
            <div class="context-dark">
                <div class="parallax-container"
                    data-parallax-img="https://www.blucactus.com.mx/wp-content/uploads/2018/11/blucactus-agencia-de-branding-para-restaurante-profesional-blucactus-1.jpg">
                    <div class="parallax-content">
                        <div class="container section-66 section-md-110">
                            <div>
                                <h1 class="font-accent"><span class="big">Menú</span></h1>
                            </div>
                            <ul
                                class="list-inline list-inline-mg list-inline-squared h3 text-uppercase text-light offset-top-4 d-none d-sm-block">
                                <li class="list-inline-item">Esta es la carta actual</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container container-wide section-66">
                <a class="banner banner-top"
                    href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/carta.php") ?>&m">
                    <img src="https://elportaldelquindio.com/imgupload/QJz4vR_BANNER%20PAG-05.jpg" height="200px">
                </a>
                <br>
                <div class="isotope-wrap">
                    <div class="row">
                        <!-- Isotope Filters-->
                        <div class="col-xl-12">
                            <div class="isotope-filters isotope-filters-horizontal">
                                <ul class="list-inline list-inline-sm">
                                    <li class="list-inline-item d-lg-none">
                                        <font color="black">
                                            <p>Elige una categoria:</p>
                                        </font>
                                    </li>
                                    <li class="list-inline-item section-relative">
                                        <button class="isotope-filters-toggle btn btn-sm btn-default"
                                            data-custom-toggle="isotope-1"
                                            data-custom-toggle-disable-on-blur="true">Filter<span
                                                class="caret"></span></button>
                                        <ul class="list-sm-inline isotope-filters-list" id="isotope-1">
                                            <li class="list-inline-item"><a class="text-sbold active"
                                                    data-isotope-filter="*" data-isotope-group="menu" href="#">Todo</a>
                                            </li>
                                            <li class="list-inline-item"><a class="text-sbold"
                                                    data-isotope-filter="Breakfasts" data-isotope-group="menu"
                                                    href="#">Andina</a></li>
                                            <li class="list-inline-item"><a class="text-sbold"
                                                    data-isotope-filter="Lunches" data-isotope-group="menu"
                                                    href="#">Llanos Orientales</a></li>
                                            <li class="list-inline-item"><a class="text-sbold"
                                                    data-isotope-filter="Dinners" data-isotope-group="menu"
                                                    href="#">Caribe</a></li>
                                            <li class="list-inline-item"><a class="text-sbold"
                                                    data-isotope-filter="Desserts" data-isotope-group="menu"
                                                    href="#">Pacifica</a></li>
                                            <li class="list-inline-item"><a class="text-sbold"
                                                    data-isotope-filter="Drinks" data-isotope-group="menu"
                                                    href="#">Amazonia</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Isotope Content-->
                        <div class="col-xl-12 offset-top-34">
                            <div class="isotope" data-isotope-layout="fitRows" data-isotope-group="menu">
                                <div class="row">
                                    <div class="col-12 col-lg-4 isotope-item" data-filter="Breakfasts">
                                        <div class="inset-lg-right-20">
                                            <!-- Pricing Box type 1-->
                                            <ul class="box-pricing box-pricing-type-1 list-unstyled">
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">Old
                                                            Timer's Breakfast</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$22.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Two eggs
                                                        cooked to order with grits, sawmill gravy, homemade buttermilk
                                                        biscuits &amp; real butter )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">Fresh
                                                            Start Sampler</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$37.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Start your
                                                        day with a mix of low fat vanilla yogurt, fresh seasonal fruit
                                                        topped with our honey mix )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Double Meat Breakfast</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$42.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Three eggs
                                                        cooked to order with a full order of bacon and sausage patties )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">Crab
                                                            &amp; avocado bruschetta</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$19.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Fresh
                                                        white&amp;brown crab, crunchy fennel, smashed avocado, yoghurt
                                                        &amp; chilli )
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 isotope-item" data-filter="Lunches">
                                        <div class="inset-lg-right-20">
                                            <!-- Pricing Box type 1-->
                                            <ul class="box-pricing box-pricing-type-1 list-unstyled">
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Turkey milanese</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$22.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Stuffed with
                                                        prosciutto &amp; provolone, breadcrumbed &amp; topped with a
                                                        free-range egg &amp; black summer truffle. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Fritto misto</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$37.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Mixed
                                                        seasonal flash-fried sustainable fish &amp; shellfish served
                                                        with chunky Italian tartare sauce &amp; lemon. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Italian steak frites</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$42.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">(
                                                        Flash-grilled prime beef steak, served with Italian-spiced
                                                        skinny fries &amp; crunchy vegetable slaw. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">Baked
                                                            Crespelle</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$19.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Spinach,
                                                        Westcombe ricotta &amp; basil baked in gluten-free buckwheat
                                                        pancakes with smoked mozzarella, sweet buttery leeks, Sicilian
                                                        tomato sauce &amp; Parmesan. )
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 isotope-item" data-filter="Dinners">
                                        <div class="inset-lg-right-20">
                                            <!-- Pricing Box type 1-->
                                            <ul class="box-pricing box-pricing-type-1 list-unstyled">
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">Our
                                                            famous prawn linguine</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$22.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Fried
                                                        garlicky prawns, fennel, tomatoes, chilli, saffron, fish broth
                                                        &amp; lemony rocket. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Gnocchi genovese</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$37.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Potato
                                                        gnocchi with green beans, crushed purple potatoes, basil pesto,
                                                        toasted pine nuts, bella lodi &amp; ricotta. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">Penne
                                                            carbonara</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$42.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Crispy
                                                        chunks of smoked pancetta, sweet buttery leeks, lemon &amp; a
                                                        traditional carbonara sauce. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Three-cheese caramelle</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$19.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Beautiful
                                                        filled pasta with ricotta, provolone, bella lodi &amp; spinach,
                                                        served with creamy tomato, garlic, basil &amp; rosé wine sauce.
                                                        )
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 isotope-item" data-filter="Desserts">
                                        <div class="inset-lg-right-20">
                                            <!-- Pricing Box type 1-->
                                            <ul class="box-pricing box-pricing-type-1 list-unstyled">
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Tiramisù</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$22.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( The classic
                                                        Italian dessert topped with chocolate shavings &amp; orange
                                                        zest. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Rasbery &amp; honeycomb pavlova</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$37.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Rippled,
                                                        gooey meringue, zesty cream, macerated raspberries, smashed
                                                        honeycomb &amp; fresh mint. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Molten chocolate praline pudding</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$42.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Warm
                                                        chocolate cake with a liquid praline centre, served with salted
                                                        caramel ice cream &amp; praline. )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Orange blossom polenta cake</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$19.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Polenta cake
                                                        studded with candied fruit, served with whipped creme fraiche
                                                        &amp; pomegranate. )
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 isotope-item" data-filter="Drinks">
                                        <div class="inset-lg-right-20">
                                            <!-- Pricing Box type 1-->
                                            <ul class="box-pricing box-pricing-type-1 list-unstyled">
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Raspberry mule</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$22.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Vodka,
                                                        raspberry purée &amp; fresh lime juice topped with ginger beer )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">La
                                                            dolce vita</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$37.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Saliza
                                                        amaretto, triple sec &amp; sugar syrup shaken up with fresh
                                                        lemon &amp; cranberry juice )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Montepulciano d’abruzzo</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$42.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Cherry &amp;
                                                        chocolate flavours are typical of this well-known grape )
                                                    </div>
                                                </li>
                                                <li class="box-pricing-item">
                                                    <div class="box-pricing-title big text-uppercase text-spacing-120">
                                                        <div class="box-pricing-name text-darker font-weight-bold">
                                                            Sauvignon blanc</div>
                                                        <div class="box-pricing-dots"></div>
                                                        <div class="box-pricing-price h5 font-weight-bold">$19.00</div>
                                                    </div>
                                                    <div class="box-pricing-desc text-dark offset-top-10">( Herby aromas
                                                        &amp; melon flavours )
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>




        <!-- Default footer-->
        <footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark">
            <div class="container">
                <div class="row justify-content-md-center text-xl-left">
                    <div class="col-md-12">
                        <div class="row justify-content-sm-center">
                            <div
                                class="col-sm-10 col-md-6 text-sm-left col-lg-6 col-xl-4 order-xl-3 order-lg-2 offset-xxl-1">
                                <h6 class="text-uppercase text-spacing-60 text-center text-lg-left">Contacto</h6>
                                <!-- RD Mailform -->
                                <form class="rd-mailform offset-top-34 offset-lg-top-0 text-left"
                                    data-form-output="form-output-global" data-form-type="contact" method="post"
                                    action="bat/rd-mailform.php">
                                    <div class="form-group">
                                        <label class="form-label form-label-sm" for="footer-v2-name">Tú Nombre:</label>
                                        <input class="form-control input-sm form-control-impressed" id="footer-v2-name"
                                            type="text" name="name" data-constraints="@Required">
                                    </div>
                                    <div class="form-group offset-top-20">
                                        <label class="form-label form-label-sm" for="footer-v2-email">Tú Email*:</label>
                                        <input class="form-control input-sm form-control-impressed" id="footer-v2-email"
                                            type="text" name="email" data-constraints="@Email @Required">
                                    </div>
                                    <div class="form-group offset-top-20">
                                        <label class="form-label form-label-sm"
                                            for="footer-v2-message">Mensaje*:</label>
                                        <textarea class="form-control input-sm form-control-impressed"
                                            id="footer-v2-message" name="message" data-constraints="@Required"
                                            style="height: 80px"></textarea>
                                    </div>
                                    <div class="group offset-top-20">
                                        <button class="btn btn-sm btn-primary" type="submit"
                                            style="padding-left: 30px; padding-right: 30px;">Enviar</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-12 offset-top-66 col-xl-3 order-xl-1 offset-xl-top-0 order-lg-3">
                                <!-- Footer brand-->
                                <div class="footer-brand"><a href="index.php">
                                        <font face="Arial black" size="6" color="white"><i class="fas fa-utensils"></i>
                                            CCC
                                        </font>
                                        <font face="Brush Script MT" size="6" color="white">Restaurant</font>
                                    </a></div>
                                <p class="text-darker offset-top-4">¡Obtén la mejor experiencia gastronómica!</p>
                                <address class="contact-info offset-top-30 p">
                                    <div>
                                        <dl>
                                            <dt class="text-white">Visitanos en:</dt>
                                            <dd class="text-dark d-xl-block">Cra 196 #67 - 34 <span
                                                    class="d-xl-block">Bogota, Colombia</span> <span
                                                    class="d-xl-block">IL
                                                    60604-2340.</span></dd>
                                        </dl>
                                    </div>
                                    <div>
                                        <dl class="offset-top-0">
                                            <dt class="text-white">Telefono:</dt>
                                            <dd class="text-dark"><a href="tel:#">+57 3013225781</a></dd>
                                        </dl>
                                    </div>
                                    <div>
                                        <dl class="offset-top-0">
                                            <dt class="text-white">E-mail:</dt>
                                            <dd class="text-dark"><a href="mailto:#">cccrestaurant@gmail.com</a></dd>
                                        </dl>
                                    </div>
                                </address>
                            </div>
                            <div
                                class="col-sm-10 col-md-6 offset-top-66 offset-lg-top-0 text-sm-left col-lg-6 col-xl-4 order-xl-2 order-lg-1">
                                <h6 class="text-uppercase text-spacing-60 text-center text-lg-left">LO QUE DICE LA GENTE
                                </h6>
                                <!-- Simple quote Slider-->
                                <div class="owl-carousel owl-carousel-classic owl-carousel-class-light owl-carousel-simple-quote veil-owl-nav"
                                    data-items="1" data-nav="false" data-dots="true">
                                    <div>
                                        <blockquote class="quote quote-simple-2 text-left">
                                            <p class="quote-body offset-bottom-0">
                                                <q>Este es uno de los mejores restaurantes que he visitado. Todo estuvo
                                                    excelente. La comida se sirvió de una manera muy creativa. Se lo
                                                    recomendaría a todos.</q>
                                            </p>
                                            <h6 class="quote-author text-uppercase text-white">
                                                <cite class="text-normal">- Leonardo DiCaprio</cite>
                                            </h6>
                                        </blockquote>
                                    </div>
                                    <div>
                                        <blockquote class="quote quote-simple-2 text-left">
                                            <p class="quote-body offset-bottom-0">
                                                <q>Su restaurante es el mejor lugar familiar para mí. Ya es una
                                                    tradición
                                                    para nosotros visitar este lugar todos los viernes por la noche.
                                                    Realmente disfruto de todo.</q>
                                            </p>
                                            <h6 class="quote-author text-uppercase text-white">
                                                <cite class="text-normal">- Emma Watson</cite>
                                            </h6>
                                        </blockquote>
                                    </div>
                                    <div>
                                        <blockquote class="quote quote-simple-2 text-left">
                                            <p class="quote-body offset-bottom-0">
                                                <q>Aquí es donde voy cada vez que me siento abrumado por la nostalgia o
                                                    simplemente quiero probar la verdadera pasta una vez más. Altamente
                                                    recomendado</q>
                                            </p>
                                            <h6 class="quote-author text-uppercase text-white">
                                                <cite class="text-normal">- Zac Efron</cite>
                                            </h6>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="offset-top-34 text-sm-center text-lg-left">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <font size="7"><a href="#"><i class="fab fa-facebook"></i></a></font>
                                        </li>
                                        <li class="list-inline-item">
                                            <font size="7"><a href="#"><i class="fab fa-twitter-square"></i></a></font>
                                        </li>
                                        <li class="list-inline-item">
                                            <font size="7"><a href="#"><i class="fab fa-google-plus"></i></a></font>
                                        </li>
                                        <li class="list-inline-item">
                                            <font size="7"><a href="#"><i class="fab fa-youtube-square"></i></a></font>
                                        </li>
                                        <li class="list-inline-item">
                                            <font size="7"><a href="#"><i class="fab fa-instagram"></i></a></font>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container offset-top-50">
                <p class="small text-darker"><span>&copy;&nbsp;</span><span
                        class="copyright-year"></span><span>&nbsp;</span><span>CCC Restaurant</span>. All Rights
                    Reserved.
                    Design by <a href="https://www.facebook.com/J.Sttiven.Borda.M/">JONATHAN BORDA</a></p>
            </div>
        </footer>
    </main>
</div>