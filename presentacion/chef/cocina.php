<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Chef") //Funcion solo para Chef
    {
?>
<?php
    //Cuando el chef reporta la preparacion de una orden:
    if (isset($_POST["preparada"]))
    {
        //Capturamos datos que se van a registrar:

        $idOrden = $_GET["idOrden"];
        $idUsuario = $_SESSION["id"];
        $estado = 4; //Orden preparada
        $fecha_hora = date("Y/m/d - H:i:s");
        $observaciones = $_POST["observaciones"];

        //Creamos el registro en la tabla proceso:

        $proceso = new Proceso ("", $idOrden, $idUsuario, $estado, $fecha_hora, $observaciones);
        $proceso -> crear();

        //Luego cambiamos el estado a 4 de esa orden del cliente:
        $orden = new Orden ($idOrden);
        $orden -> editar2(4); //Estado 4 -> Preparada (Orden preparada por el chef)
    }

    //Primero busco todas las ordenes que hayan sido recibidas por el chef de la sesion:
    $orden = new Orden();
    $arregloOrdenes = $orden -> consultarTodos3(3, $_SESSION["id"]);

?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Ordenes en la cocina</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br>
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h4 class="text-left">- Estas son las ordenes que estas atendiendo:</h4>
                    </div>
                </div>
                
                <!-- Para avisar cuando se atiende una orden -->
                <?php
                    if(isset($_GET["idOrden"]))
                    { 
                ?>  
                    <br>
                    <section id="alert2"> 
                        <div class="alerta">        
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Genial: Has finalizado la preparación de la orden!</strong> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div> 
                    </section>         
                <?php 
                    }
                ?>
                <br><br>
                <table class="table table-warning table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Platos</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Observaciones</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Acción</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        foreach ($arregloOrdenes as $pedidoActual)
                        {
                            if ($pedidoActual->getEstado() == 3)
                            {
                                //Busco la bandeja de platos que tiene registrada esa orden
                                $bandeja = new Bandeja ($pedidoActual->getIdOrden());
                                
                                //Me trae un array de todos los registros asociados a esa orden
                                $arregloBandeja = $bandeja -> consultarTodos(); 
                                
                                $unidadesTotal=0;
                                //Para calcular cuantos platos pidio en esa orden
                                foreach ($arregloBandeja as $carritoActual)
                                {
                                    $unidadesTotal += $carritoActual -> getCantidad_und();
                                }
                                
                                //Muestro los datos de la orden que necesito:
                                echo "<tr>";
                                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdOrden() . "</font></h5></td>";             	                     	        
                                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                                    if ($pedidoActual->getObs() == "")
                                    {
                                        echo "<td class='text-center'><h5><font face='Arial' size='2' color='black'>No</font></h5></td>";
                                    }
                                    else
                                    {
                                        echo "<td class='text-center'><h5><font face='Arial' size='2' color='black'>" . $pedidoActual->getObs() . "</font></h5></td>";                                 
                                    }
                                    echo "<td class='text-center'><a href='modalObs.php?idOrden=" . $pedidoActual->getIdOrden() . "'
                                        data-toggle='modal' data-target='#modalObs'><font size='6' color='red'><font size='6' color='red'><i class='far fa-check-circle' data-toggle='tooltip' data-placement='bottom' title='Preparada'></i></font></a></td>";                                
                                echo "</tr>";      
                            }                              	   
                        }     
            	    ?>
                    </tbody>
                </table>
                <br>

                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a href="index.php?pid=<?php echo base64_encode("presentacion/chef/cocina.php")?>&m"
                                    class='btn float-right login_btn3'>
                                    ACTUALIZAR
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>
<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh"
    content="0;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>" />
<?php
}
?>


<!--Este es el Modal para las observaciones: -->
<section id="modalO">	
    <div class="modal fade" id="modalObs" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>    

<!--Este JavaScript es para el modal: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>