
<!--==========================
      Sección de encabezado en menu
      ============================-->

<header class="page-head slider-menu-position">
    <!-- RD Navbar Transparent (Menu transparente)-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-default rd-navbar-transparent" data-auto-height="true"
            data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                    <!--Navbar Brand-->
                    <div class="rd-navbar-brand"><a href="index.php">
                            <font face="Arial black" size="6" color="white"><i class="fas fa-utensils"></i> CC</font>
                            <font face="Brush Script MT" size="6" color="white">Restaurant</font>
                        </a></div>
                </div>
                <div class="rd-navbar-menu-wrap">
                    <div class="rd-navbar-nav-wrap">
                        <div class="rd-navbar-mobile-scroll">
                            <!--Navbar Brand Mobile-->
                            <div class="rd-navbar-mobile-brand"><a href="index.html"><a href="index.php">
                                        <font face="Arial black" size="6" color="white"><i class="fas fa-utensils"></i>
                                            CC</font>
                                        <font face="Brush Script MT" size="6" color="white">Restaurant</font>
                                    </a></div>
                            <div class="form-search-wrap">
                                <form class="form-search" action="#" method="GET">
                                    <div class="form-group">
                                        <label class="form-label form-search-label form-label-sm"
                                            for="rd-navbar-form-search-widget">Buscar</label>
                                        <input
                                            class="form-search-input input-sm form-control form-control-gray-lightest input-sm"
                                            id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off" />
                                    </div>
                                    <button class="form-search-submit" type="submit"><span
                                            class="mdi mdi-magnify"></span></button>
                                </form>
                            </div>
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                <li class="active"><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/chef/sesionChef.php") ?>&m"><span>Inicio</span></a>
                                </li>
                                <li><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/carta.php") ?>&m"><span>Carta</span></a>
                                </li>
                                <li><a href="index.php?pid=<?php echo base64_encode("presentacion/chef/ordenes.php") ?>&m"><span>Ordenes</span></a>
                                </li>
                                <li><a href="index.php?pid=<?php echo base64_encode("presentacion/chef/cocina.php") ?>&m"><span>Cocina</span></a>
                                </li>
                                <li><a href="index.php?pid=<?php echo base64_encode("presentacion/log.php")?>&m">Log</a>
                                </li>
                                <li><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>&session=0">
                                        <span>Cerrar Sesion</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>