<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Mesero") //Funcion solo para Mesero
    {
?>
<?php
    //Cuando el chef quiera atender una orden:
    if (isset($_GET["idOrden"]))
    {
        //Capturamos datos que se van a registrar:

        $idOrden = $_GET["idOrden"];
        $idUsuario = $_SESSION["id"];
        $estado = 6; //Orden entregada
        $fecha_hora = date("Y/m/d - H:i:s");

        //Creamos el registro en la tabla proceso:

        $proceso = new Proceso ("", $idOrden, $idUsuario, $estado, $fecha_hora);
        $proceso -> crear();

        //Luego cambiamos el estado a 6 de esa orden del cliente:
        $orden = new Orden ($idOrden);
        $orden -> editar2(6); //Estado 6 -> Entregada (al cliente):
    }

    //Primero busco todas las ordenes existentes
    $orden = new Orden();
    $arregloOrdenes = $orden -> consultarTodos2();

?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Ordenes por entregar</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br>
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h4 class="text-left">- Estas son las ordenes que estan preparadas, entrega una!</h4>
                    </div>
                </div>
                
                <!-- Para avisar cuando se atiende una orden -->
                <?php
                    if(isset($_GET["idOrden"]))
                    { 
                ?>  
                    <br>
                    <section id="alert2"> 
                        <div class="alerta">        
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Se ha registrado la entrega de la orden correctamente!</strong> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div> 
                    </section>         
                <?php 
                    }
                ?>
                <br><br>
                <table class="table table-warning table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Platos</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Acción</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                    foreach ($arregloOrdenes as $pedidoActual)
                	{   
                        if ($pedidoActual->getEstado() == 4) //Le va mostrar las ordenes que estan preparadas
                        {
                            //Busco la bandeja de platos que tiene registrada esa orden
                            $bandeja = new Bandeja ($pedidoActual->getIdOrden());
                            
                            //Me trae un array de todos los registros asociados a esa orden
                            $arregloBandeja = $bandeja -> consultarTodos(); 
                            
                            $unidadesTotal=0;
                            //Para calcular cuantos platos pidio en esa orden
                            foreach ($arregloBandeja as $carritoActual)
                            {
                                $unidadesTotal += $carritoActual -> getCantidad_und();
                            }
                            
                            //Muestro los datos de la orden que necesito:
                            echo "<tr>";
                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdOrden() . "</font></h5></td>";             	        
                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getFecha_hora() . "</font></h5></td>";
                                echo "<td class='text-center'><font face='Arial' size='3' color='black'>$" . number_format($pedidoActual->getValor_total(), ...array(0, ',', '.')) . "</font></td>";               	        
                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                                echo "<td class='text-center'><a href='index.php?pid=" . base64_encode("presentacion/mesero/ordenesPorEntregar.php") . "&m&idOrden=" . $pedidoActual->getIdOrden() . "'><font size='6' color='red'><i class='far fa-check-circle' data-toggle='tooltip' data-placement='bottom' title='Entregada'></i></font></a></td>";                                
                            echo "</tr>";          
                        }      	   
                	}     
            	?>
                    </tbody>
                </table>
                <br>

                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a href="index.php?pid=<?php echo base64_encode("presentacion/mesero/ordenesPorEntregar.php")?>&m"
                                    class='btn float-right login_btn3'>
                                    ACTUALIZAR
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>
<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh"
    content="0;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>" />
<?php
}
?>