<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">
                                <font color="black">Estadisticas de
                                    las Ventas del Restaurante</font>
                            </span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>

                <br><br>

                <div class="row justify-content-xl-left">
                    <div class="col-xl-12">
                        <h1 class="text-left">
                            <font style="background-color:yellow;" size="5" color="black">1. Aqui puedes consultar ventas mensual y anualmente en dinero
                                y en cantidad de platos:
                            </font>
                        </h1>
                    </div>
                </div>
                <br><br>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Año</label>
                        </div>
                        <select class="custom-select" id="edition">
                            <option value="-1">Seleccionar año</option>
                            <option value="2021">2021</option>
                            <option value="2020">2020</option>
                            <option value="2019">2019</option>
                        </select>

                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div id="resultados">

                        </div>
                    </div>
                </div>
            </div>


            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-left">
                    <div class="col-xl-12">
                        <h1 class="text-left">
                            <font  style="background-color:yellow;" size="5" color="black">
                                2. El plato más vendido de la carta es:
                            </font>
                        </h1>
                    </div>
                </div>
                <?php 
                    $plato = new Plato(); 
                    $arregloPlatos = $plato -> platoMasVendido();
                    $datos = "['Plato', 'Vendidos'], ";
                
                    foreach ($arregloPlatos as $l)
                    {
                        $datos .= "['" . trim($l[0]) . "', " . $l[1] . "], ";
                    }
                ?>

                <div class="row">
                    <div class="col-xl-12">
                        <!-- Luego de añadir la libreria al index, el siguiente paso es identifacr la capa en donde se mostrará
                                los graficos, en este caso en el siguiente div => id="piechartCantidad" -->
                        <div class="text-center" id="piechartCantidad" style="width: 840px; height: 500px;"></div>
                    </div>
                </div>
            </div>


            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-left">
                    <div class="col-xl-12">
                        <h1 class="text-left">
                            <font  style="background-color:yellow;" size="5" color="black">
                                3. Cantidad de platos en cada carta existente:
                            </font>
                        </h1>
                    </div>
                </div>
                <?php 
                    $carta = new Carta(); 
                    $arregloCartas = $carta -> platosEnCarta();
                    $datos2 = "['Carta', 'Cantidad Platos'], ";
                
                    foreach ($arregloCartas as $l)
                    {
                        $datos2 .= "['" . trim($l[0]) . "', " . $l[1] . "], ";
                    }
                ?>

                <div class="row">
                    <div class="col-12">
                        <!-- Luego de añadir la libreria al index, el siguiente paso es identifacr la capa en donde se mostrará
                                los graficos, en este caso en el siguiente div => id="piechartCantidad" -->
                        <div class="text-center" id="piechart2" style="width: 740px; height: 500px;"></div>
                    </div>
                </div>
            </div>


            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-left">
                    <div class="col-xl-12">
                        <h1 class="text-left">
                            <font  style="background-color:yellow;" size="5" color="black">
                                4. Ingredientes en platos:
                            </font>
                        </h1>
                    </div>
                </div>
                <?php 
                    $ingre = new Ingrediente(); 
                    $arregloIngre = $ingre -> ingrePlato();
                    $datos3 = "['Plato', 'Ingredientes'], ";
                
                    foreach ($arregloIngre as $l)
                    {
                        $datos3 .= "['" . trim($l[0]) . "', " . $l[1] . "], ";
                    }
                ?>

                <div class="row">
                    <div class="col-12">
                        <!-- Luego de añadir la libreria al index, el siguiente paso es identifacr la capa en donde se mostrará
                                los graficos, en este caso en el siguiente div => id="piechartCantidad" -->
                        <div class="text-center" id="piechart3" style="width: 1200px; height: 600px;"></div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>

<!-- Script para grafico 2 Piechart -->

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    
    function drawChart() 
    {
    
        var data = google.visualization.arrayToDataTable([
            <?php echo $datos ?>
            
        ]);
        
        var options = {
        	title : "Ventas por plato",
            is3D: true,
        };                            
        var chart = new google.visualization.PieChart(document.getElementById('piechartCantidad'));                                
        chart.draw(data, options);
    }
</script>

<!-- Script para grafico 3 Piechart -->

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    
    function drawChart() 
    {
    
        var data = google.visualization.arrayToDataTable([
            <?php echo $datos2 ?>
            
        ]);
        
        var options = {
        	title : "Platos en carta",
            pieHole: 0.4,
        };                            
        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));                                
        chart.draw(data, options);
    }
</script>


<!-- Script para grafico 4 ColumnChart -->

<script type="text/javascript">
google.charts.load('current', {
    'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() 
{

    var data = google.visualization.arrayToDataTable([
        <?php echo $datos3 ?>

    ]);

    var options = {
        title: "Cantidad de ingredientes por plato"
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('piechart3'));
    chart.draw(data, options);
}
</script>




<!--Este es el JQuery: -->
<script>
$(document).ready(function() {
    $("#edition").change(function() {
        if ($("#edition").val() != -1) {
            //Encriptamos la ruta para que no sea visible en la inspección de codigo:
            url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/estadisticasVentasAjax.php") ?>&edition=" +
                $("#edition").val();
            $("#resultados").load(url);
        }
    });
});
</script>