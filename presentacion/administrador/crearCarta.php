<?php 
    //Para crear carta:
    if (isset($_POST["crearC"]))
    {
        $error=0;
        //Estos son los datos que llegan por el formulario cuando se quiere crear:
        $nombre = $_POST["nombre"];
        $vigencia = $_POST["vigencia"];

        //Primero se valida que el nombre de la carta no exista:
        $carta = new Carta ("", $nombre);
        if ($carta -> consultar())
        {
            //Si existe el nombre en la BD:
            $error = 1;
        }

        if ($error == 0) //Si no hay error en el nombre de la carta, pasa:
        {
            //Envio esos datos que tendra la carta:
            $carta = new Carta ("", $nombre, $vigencia);
            $carta -> crear();
        }     
    }
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Creando Nuevo Menú
                            </font>
                        </h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h1 align="left">
                            <font face="Arial Black" size="4">
                                PARAMETROS A TENER EN CUENTA:
                            </font>
                        </h1>
                    </div>
                </div>

                <div class="row-mt">
                    <div class="col-md-12">
                        <p align="left">
                            <mark style="background-color: #ff0000;">
                                <font face="Arial Black" size="3">
                                    1. Crear una carta<br>
                                </font>
                            </mark>
                            <font face="Arial Black" size="3">
                                2. Crear platos y agregarlos a la carta<br>
                                3. Crear ingredientes que son agregados al plato
                            </font>
                        </p>
                    </div>
                </div>
            </div>

            <br><br><br><br>

            <div class="container wow fadeInUp">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">CARTA</h3>
                    </div>
                    <!-- Para avisar cuando se crea la carta -->
                    <?php
                            if(isset($_POST["crearC"]) && $error == 0)
                            { 
                    ?>
                    <section id="alert2">
                        <div class="alerta">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Genial: Carta creada con éxito!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>

                    <!-- Para reporatar error en el nombre de la carta-->
                    <?php
                        if (isset($_POST["crearC"]) && $error == 1)
                        { 
                    ?>
                    <section id="alert1">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> Error: El nombre de la carta ya existe!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }               
                    ?>
                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/crearCarta.php") . "&m"?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Nombre:</h4>
                                <input type="text" name="nombre" class="form-control"
                                    placeholder="Escribe el nombre de la carta" required="required">
                            </div>
                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Vigencia: </h4>
                                <input type="text" name="vigencia" class="form-control" placeholder="Ej: 3 meses"
                                    required="required">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <input type="submit" name="crearC" value="Crear" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/administrador/sesionAdministrador.php")?>&m">
                                Salir</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>

<?php
    //Si crea un plato lo redirige a los ingredientes:
    if(isset($_POST["crearC"]) && $error == 0)
    { 
?>
<meta http-equiv="refresh"
    content="4;url=index.php?pid=<?php echo base64_encode("presentacion/administrador/crearPlato.php")?>&m&n" />
<?php 
    }
?>