<?php 
    $idCarta = $_GET["idCar"];
    $c = new Carta ($idCarta);
    $c -> consultar ();
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio"
            style="background-image: url(https://s1.1zoom.me/big0/263/Colombia_Scenery_Rivers_Coast_Houses_Clouds_Hill_529353_1280x850.jpg)">
            <div class="container wow fadeInUp">
                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a class="btn btn-lg btn-primary" href="#">
                                    <font face="Goudy Stout" size="5">
                                        <?php echo $c -> getNombre(); ?>
                                    </font>
                                </a>
                            </div>
                            <hr class="divider bg-mantis offset-top-30">
                        </div>
                    </div>
                </div>
            </div>



            <br><br>

            <!-- Aqui empiezan a mostrarse las regiones:  -->
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card2">
                            <a class="portfolio-item"
                                style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-4-728.jpg?cb=1315044688);"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=2&idCar=<?php echo $_GET["idCar"]?>">
                                <div class="details">
                                    <h3 class='card-title'>
                                        <font size="6">CCC Restaurant</font>
                                    </h3>
                                    <br>
                                    <h2 class="card-text">
                                        <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                    </h2>
                                </div>
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <font>Amazonía</font>
                                </h4>
                                <h3 class="card-text">
                                    <font face="Arial">
                                        <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 2);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                    </font>
                                </h3>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="card2">
                            <a class="portfolio-item"
                                style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-3-728.jpg?cb=1315044688);"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=1&idCar=<?php echo $_GET["idCar"]?>">
                                <div class="details">
                                    <h3 class='card-title'>
                                        <font size="6">CCC Restaurant</font>
                                    </h3>
                                    <br>
                                    <h2 class="card-text">
                                        <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                    </h2>
                                </div>
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <font>Andina</font>
                                </h4>
                                <h3 class="card-text">
                                    <font face="Arial">
                                        <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 1);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                    </font>
                                </h3>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="card2">
                            <a class="portfolio-item"
                                style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-1-728.jpg?cb=1315044688);"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=3&idCar=<?php echo $_GET["idCar"]?>">
                                <div class="details">
                                    <h3 class='card-title'>
                                        <font size="6">CCC Restaurant</font>
                                    </h3>
                                    <br>
                                    <h2 class="card-text">
                                        <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                    </h2>
                                </div>
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <font>Caribe</font>
                                </h4>
                                <h3 class="card-text">
                                    <font face="Arial">
                                        <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 3);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                    </font>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="card2">
                            <a class="portfolio-item"
                                style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-5-728.jpg?cb=1315044688);"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=4&idCar=<?php echo $_GET["idCar"]?>">
                                <div class="details">
                                    <h3 class='card-title'>
                                        <font size="6">CCC Restaurant</font>
                                    </h3>
                                    <br>
                                    <h2 class="card-text">
                                        <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                    </h2>
                                </div>
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <font>Llanos Orientales</font>
                                </h4>
                                <h3 class="card-text">
                                    <font face="Arial">
                                        <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 4);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                    </font>
                                </h3>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="card2">
                            <a class="portfolio-item"
                                style="background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzUMrwcQ_S4zQr4rBG9ATDu9kk_ZakuerZMg&usqp=CAU);"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=5&idCar=<?php echo $_GET["idCar"]?>">
                                <div class="details">
                                    <h3 class='card-title'>
                                        <font size="6">CCC Restaurant</font>
                                    </h3>
                                    <br>
                                    <h2 class="card-text">
                                        <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                    </h2>
                                </div>
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <font>Pacifica</font>
                                </h4>
                                <h3 class="card-text">
                                    <font face="Arial">
                                        <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 5);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                    </font>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>


                <br><br>
                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a class="btn btn-lg btn-primary"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/administrador/gestionarCarta.php")?>&m">Atras</a>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </section>
</main>