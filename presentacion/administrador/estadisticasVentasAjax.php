<?php
if($_SESSION["rol"] == "Administrador")
{
    $orden = new Orden(); 
    $ventasPorMes = $orden -> consultarVentasPorMes($_GET["edition"]);
    $datos = "['Mes', 'Total Ventas'], ";
    $datos2 = "['Mes', 'Total Platos'], ";

    foreach ($ventasPorMes as $l)
    {
        $datos .= "['" . trim($l[0]) . "', " . $l[1] . "], ";
    }

    foreach ($ventasPorMes as $l)
    {
        $datos2 .= "['" . trim($l[0]) . "', " . $l[2] . ",], ";
    }
?>
<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <!-- Luego de añadir la libreria al index, el siguiente paso es identifacr la capa en donde se mostrará
                                los graficos, en este caso en el siguiente div => id="piechartCantidad" -->
                        <div class="text-center" id="piechartCantidad" style="width: 740px; height: 500px;">
                        </div>
                        <br>
                        <div class="text-center" id="piechartCantidad2" style="width: 740px; height: 500px;">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>
<?php 
} 
else 
{
    echo "Lo siento. Usted no tiene permisos";
}
?>


<script type="text/javascript">
google.charts.load('current', {
    'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
        <?php echo $datos ?>

    ]);

    var options = {
        title: "Ventas en dinero mensuales año " + <?php echo $_GET["edition"] ?>

    };
    var chart = new google.visualization.ColumnChart(document.getElementById('piechartCantidad'));
    chart.draw(data, options);

    var data2 = google.visualization.arrayToDataTable([
        <?php echo $datos2 ?>

    ]);

    var options2 = {
        title: "Platos vendidos mensuales año "  + <?php echo $_GET["edition"] ?>
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('piechartCantidad2'));
    chart.draw(data2, options2);
}
</script>