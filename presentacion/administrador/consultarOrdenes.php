<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admin
    {
?>
<?php
    //Consulto todas las ordenes que estan registradas en el restaurante
    $orden = new Orden ();
    $arregloOrdenes = $orden -> consultarTodos2();
?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Ordenes del restaurante</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>
                <table class="table table-warning table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Platos</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Cliente</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Acción</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        foreach ($arregloOrdenes as $pedidoActual)
                        {   
                            //Busco la bandeja de platos que tiene registrada esa orden
                            $bandeja = new Bandeja ($pedidoActual->getIdOrden());
                            
                            //Me trae un array de todos los registros asociados a esa orden
                            $arregloBandeja = $bandeja -> consultarTodos(); 
                            
                            $unidadesTotal=0;
                            //Para calcular cuantos platos pidio en esa orden
                            foreach ($arregloBandeja as $carritoActual)
                            {
                                $unidadesTotal += $carritoActual -> getCantidad_und();
                            }
                            
                            //Muestro los datos de la orden que necesito:
                            echo "<tr>";
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdOrden() . "</font></h5></td>";             	        
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getFecha_hora() . "</font></h5></td>";
                            echo "<td class='text-center'><font face='Arial' size='3' color='black'>$" . number_format($pedidoActual->getValor_total(), ...array(0, ',', '.')) . "</font></td>";               	        
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdUsuario() . "</font></h5></td>"; 
                            echo "<td class='text-center'><a href='crearPdf.php?idOrden=" . $pedidoActual->getIdOrden() . "&idUsu=" . $pedidoActual->getIdUsuario() . "'><font face='Arial' size='3' color='black'><i class='fas fa-file-download' data-toggle='tooltip' data-placement='bottom' title='Descargar Reporte'></i></font></a></td>";               	                    	       
                        echo "</tr>";                	   
                        }              	
            	    ?>
                    </tbody>
                </table>
                <br>

                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarOrdenes.php")?>&m"
                                    class='btn float-right login_btn3'>
                                    ACTUALIZAR
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>
<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh"
    content="0;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>" />
<?php
}
?>