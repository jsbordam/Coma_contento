<?php 
    $carta = new Carta();
    $cartas = $carta -> consultarTodos();

    //Actualizo la carta activa del restaurante:

    //Primero deshabilito la carta que esta activa en el momento
    foreach ($cartas as $cartaActual)
    {
        if ($cartaActual -> getEstado() == 1)
        {
            $c = new Carta ($cartaActual->getIdCarta(), "", "", 0);
            $c -> activa ();
        }
    }

    //Luego si activo la que se desea:
    $c = new Carta ($_GET["activa"], "", "", 1);
    $c -> activa ();

    //Consulto los datos actualizados de todas las cartas:
    $cartas = $c -> consultarTodos();


    foreach ($cartas as $cartaActual)
    {   			     			        
        echo "<div class='col-md-4 col-xl-4'>";
            echo "<br>";
            if ($cartaActual -> getEstado() == 1)
            {
                //Carta activa en el restaurante:
                echo "<div class='card3'>";
            }
            else
            {
                echo "<div class='card2'>";
            }
                echo "<a class='portfolio-item'
                            style='background-image: url(https://www.bahiagrafica.com/wp-content/uploads/2018/12/dise%C3%B1o-cartas.jpg);'
                            href='index.php?pid=" . base64_encode("presentacion/administrador/consultarCarta.php") . "&m&idCar=" . $cartaActual -> getIdCarta() . "'" . ">";
                echo "<div class='details'>";
                    echo "<h4>";
                        echo "<font>" . $cartaActual -> getIdCarta() . "</font>";
                    echo "</h4>";
                    echo "<h3 class='card-title'>";
                        echo "<font>" . $cartaActual -> getNombre() . "</font>";
                    echo "</h3>";
                    echo "<br>";
                    echo "<h2 class='card-text'>";
                        echo "<font face='Arial Black'><i class='fas fa-eye'></i> Ver Carta</font>";
                    echo "</h2>";
                echo "</div>";
                echo "</a>";
                echo "<div class='card-body'>";
                    echo "<h4 align ='center'>";
                        echo "<font face='Brush Script MT' color='White' size='6'><a href='index.php?pid=" . base64_encode("presentacion/administrador/editarCarta.php") . "&m&idCar=" . $cartaActual -> getIdCarta() . "'><i class='far fa-edit' data-toggle='tooltip' data-placement='bottom' title='Editar Carta'></i></a> Vigencia de " . $cartaActual -> getVigencia() . "</font>";
                    echo "</h4>";
                echo "</div>";
            echo "</div>";       			        
        echo "</div>";
    } 
?>