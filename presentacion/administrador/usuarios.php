<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">
                                <font color="black">OTROS DATOS</font>
                            </span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>

                <br><br><br>

                <div class="row justify-content-xl-left">
                    <div class="col-xl-12">
                        <h1 class="text-left">
                                <font size="5" color="black">Ordenes con estado SELECCIONANDO:
                                </font>
                        </h1>    
                    </div>
                    <table class="table table-danger table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Codigo</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Estado</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Usuario</font>
                                    </h5>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                                $orden = new Orden ();
                                $seleccionadas = $orden -> llamarProce1 (1);

                                foreach ($seleccionadas as $se)
                                {
                                    //Muestro los datos de la orden que necesito:
                                    echo "<tr>";
                                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getIdOrden() . "</font></h5></td>";
                                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getFecha_hora() . "</font></h5></td>";
                                    echo "<td class='text-center'><font face='Arial' size='3' color='black'>" . $orden->getValor_total() . "</font></td>";
                                    echo "<td class='text-center'><font face='Arial' size='3' color='black'>" . $orden->getEstado() . "</font></td>";     
                                }                                                  	      	                   	                         	
                            ?>
                            </tbody>
                    </table>


                    <table class="table table-danger table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Codigo</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Estado</font>
                                    </h5>
                                </th>
                                <th class="text-center">
                                    <h5>
                                        <font face='Arial Black' size='4' color='black'>Usuario</font>
                                    </h5>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                                $orden = new Orden ();
                                $solicitadas = $orden -> llamarProce1 (2);

                                foreach ($solicitadas as $se)
                                {
                                    //Muestro los datos de la orden que necesito:
                                    echo "<tr>";
                                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getIdOrden() . "</font></h5></td>";
                                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getFecha_hora() . "</font></h5></td>";
                                    echo "<td class='text-center'><font face='Arial' size='3' color='black'>" . $orden->getValor_total() . "</font></td>";
                                    echo "<td class='text-center'><font face='Arial' size='3' color='black'>" . $orden->getEstado() . "</font></td>";     
                                }                                                  	      	                   	                         	
                            ?>
                            </tbody>
                    </table>
                </div>
                <br><br>
            </div>
        </section>
    </section>
</main>