<?php 
    //Consulto todas las cartas que hayan:
    $carta = new Carta ();
    $cartas = $carta -> consultarTodos();
?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Gestionando Menú
                            </font>
                        </h1>
                    </div>
                </div>

                <br><br>
                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a class="btn btn-lg btn-primary" href="#">Estas son las cartas del restaurante</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <h1 align="left">
                            <font size="5" style="color: rgb(91, 238, 7) !important;"><i class="fas fa-circle"></i></font>
                            <font face="Brush Script MT" size="7"> Carta activa</font>
                        </h1>
                    </div>
                </div>

                

                <div class="row">                
                    <div class="col-md-4">
                        <select class="custom-select" id="activa">
                            <option value="-1">Cambiar carta activa</option>
                            <?php 
                                foreach ($cartas as $c)
                                {
                                    echo "<option value='" . $c->getIdCarta() . "'>" . $c->getNombre() . "</option>";
                                }       
                            ?>
                        </select>
                    </div>
                </div>

                <div id="resultados" class="row">
                    <?php	
                        foreach ($cartas as $cartaActual)
                        {   			     			        
                            echo "<div class='col-md-4 col-xl-4'>";
                                echo "<br>";

                                if ($cartaActual -> getEstado() == 1)
                                {
                                    //Carta activa en el restaurante:
                                    echo "<div class='card3'>";
                                }
                                else
                                {
                                    echo "<div class='card2'>";
                                }
                                    echo "<a class='portfolio-item'
                                                style='background-image: url(https://www.bahiagrafica.com/wp-content/uploads/2018/12/dise%C3%B1o-cartas.jpg);'
                                                href='index.php?pid=" . base64_encode("presentacion/administrador/consultarCarta.php") . "&m&idCar=" . $cartaActual -> getIdCarta() . "'" . ">";
                                    echo "<div class='details'>";
                                        echo "<h4>";
                                            echo "<font>" . $cartaActual -> getIdCarta() . "</font>";
                                        echo "</h4>";
                                        echo "<h3 class='card-title'>";
                                            echo "<font>" . $cartaActual -> getNombre() . "</font>";
                                        echo "</h3>";
                                        echo "<br>";
                                        echo "<h2 class='card-text'>";
                                            echo "<font face='Arial Black'><i class='fas fa-eye'></i> Ver Carta</font>";
                                        echo "</h2>";
                                    echo "</div>";
                                    echo "</a>";
                                    echo "<div class='card-body'>";
                                        echo "<h4 align ='center'>";
                                            echo "<font face='Brush Script MT' color='White' size='6'><a href='index.php?pid=" . base64_encode("presentacion/administrador/editarCarta.php") . "&m&idCar=" . $cartaActual -> getIdCarta() . "'><i class='far fa-edit' data-toggle='tooltip' data-placement='bottom' title='Editar Carta'></i></a> Vigencia de " . $cartaActual -> getVigencia() . "</font>";
                                        echo "</h4>";
                                    echo "</div>";
                                echo "</div>";       			        
                            echo "</div>";
                        } 
                    ?>
                </div>

                <br><br>
                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a class="btn btn-lg btn-primary"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/administrador/sesionAdministrador.php")?>&m">Salir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>

<!--Este es el JQuery: -->
<script>
$(document).ready(function() {
    $("#activa").change(function() {
        if ($("#activa").val() != -1)
        {
            //Encriptamos la ruta para que no sea visible en la inspección de codigo:
            url =
                "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cartaActivaAjax.php") ?>&activa=" + $("#activa").val();
            $("#resultados").load(url);
        }
    });
});
</script>