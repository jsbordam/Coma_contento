<?php 
    $c = new Carta ($_GET["idCar"]);
    $c -> consultar ();

    //Para crear carta:
    if (isset($_POST["editar"]))
    {
        $error=0;
        //Estos son los datos que llegan por el formulario cuando se quiere editar:
        $nombre = $_POST["nombre"];
        $vigencia = $_POST["vigencia"];

        //Primero se valida que el nombre de la carta no exista:
        //Pero hay una excepcion y es si esta editando y quiere dejar el mismo nombre:

        if ($c -> getNombre() != $nombre)
        {
            //Si ingresa un nombre diferente al actual entonces si revisa
            $carta = new Carta ("", $nombre);
            if ($carta -> consultar())
            {
                //Si existe el nombre en la BD:
                $error = 1;
            }
        }

        

        if ($error == 0) //Si no hay error en el nombre de la carta, pasa:
        {
            //Envio esos datos que tendra la carta modificada:
            $carta = new Carta ($_GET["idCar"], $nombre, $vigencia);
            $carta -> editar();
        }     
    }
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Editando Carta
                            </font>
                        </h1>
                    </div>
                </div>
            </div>

            <br><br><br><br>

            <div class="container wow fadeInUp">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><?php echo $c -> getNombre(); ?></h3>
                    </div>
                    <!-- Para avisar cuando se edita la carta -->
                    <?php
                            if(isset($_POST["editar"]) && $error == 0)
                            { 
                    ?>
                    <section id="alert2">
                        <div class="alerta">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Genial: La carta ha sido actualizada con éxito!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>

                    <!-- Para reporatar error en el nombre de la carta-->
                    <?php
                        if (isset($_POST["editar"]) && $error == 1)
                        { 
                    ?>
                    <section id="alert1">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> Error: El nombre de la carta ya existe!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }               
                    ?>
                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/editarCarta.php") . "&m&idCar=" . $_GET["idCar"] . ""?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Nuevo nombre: </h4>
                                <input type="text" name="nombre" class="form-control"
                                    value="<?php echo $c -> getNombre(); ?>" required="required">
                            </div>
                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Nueva vigencia: </h4>
                                <input type="text" name="vigencia" class="form-control" value="<?php echo $c -> getVigencia(); ?>"
                                    required="required">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <input type="submit" name="editar" value="Guardar" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/administrador/gestionarCarta.php")?>&m">
                                Atras</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>