<?php 
    //Para crear carta:
    if (isset($_POST["agregar"]))
    {
        //Estos son los datos que llegan por el formulario cuando se quiere crear:
        $nombre = $_POST["nombre"];
        $cantidad = $_POST["cantidad"];
        $und_medida = $_POST["unidad_medida"];

        //Envio esos datos que tendra el ingrediente:
        $ingrediente = new Ingrediente ("", $nombre, $cantidad, $und_medida);
        $ingrediente -> crear();  

        //Ahora se le agrega ese ingrediente al plato, es decir la relacion:

        //Para saber el id del ingrediente que se inserto por ultima vez, que es 
        //el que neceistamos guardar en plato:
        $ingrediente -> ultimoId();

        //Ahora si tenemos todos los datos para relacionar:
        $relacion = new Ingrediente_Plato ($ingrediente->getIdIngrediente(), $_GET["idPlato"]);
        $relacion -> crear ();
    }
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Creando Nuevo Menú
                            </font>
                        </h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h1 align="left">
                            <font face="Arial Black" size="4">
                                PARAMETROS A TENER EN CUENTA:
                            </font>
                        </h1>
                    </div>
                </div>

                <div class="row-mt">
                    <div class="col-md-12">
                        <p align="left">
                            <font face="Arial Black" size="3">
                                1. Crear una carta<br>
                                2. Crear platos y agregarlos a la carta<br>
                            </font>
                            <mark style="background-color: #ff0000;">
                                <font face="Arial Black" size="3">
                                    3. Crear ingredientes que son agregados al plato
                                </font>
                            </mark>
                        </p>
                    </div>
                </div>
            </div>

            <br><br><br><br>

            <div class="container wow fadeInUp">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">INGREDIENTES DEL PLATO <br>
                            <font face="arial"><?php echo "(" . $_GET["n"] . ")"; ?></font>
                        </h3>
                    </div>
                    <!-- Para avisar que debe ingresar ingredientes de ese plato -->
                    <?php
                        if(isset($_GET["n"]))
                        { 
                    ?>
                    <section id="alert3">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-edit"></i> Ahora ingresa los ingredientes que tiene el
                                plato</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>

                    <!-- Para avisar cuando se crea el ingrediente -->
                    <?php
                        if(isset($_POST["agregar"]))
                        { 
                    ?>
                    <section id="alert2">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-check-circle"></i> Genial: Ingrediente agregado al plato con
                                éxito!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>
                    <br>
                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/crearIngre.php") . "&m&idPlato=" . $_GET["idPlato"] . "&n=" . $_GET["n"] . ""?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="nombre" class="form-control" placeholder="Nombre"
                                    required="required">
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="number" name="cantidad" class="form-control" placeholder="Cantidad"
                                    required="required">
                            </div>

                            <div class="form-group col-sm-4">
                                <select id="inputState" class="form-control" name="unidad_medida">
                                    <option value="-1">Unidad de medida</option>
                                    <option value="1">Libras</option>
                                    <option value="2">Kilos</option>
                                    <option value="3">Tazas</option>
                                    <option value="4">Unidades</option>
                                    <option value="5">Gramos</option>
                                    <option value="6">Cucharadas</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <input type="submit" name="agregar" value="Agregar" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearPlato.php")?>&m&terminado">
                                Terminar</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>