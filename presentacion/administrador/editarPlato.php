<?php 
    $p = new Plato ($_GET["idP"]);
    $p -> consultar ();

    //Para editar plato:
    if (isset($_POST["editar"]))
    {
        $error=0;
        //Estos son los datos que llegan por el formulario cuando se quiere crear:
        $nombre = $_POST["nombre"];
        $descripcion = $_POST["descripcion"];


        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
            
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "img/platos/" . time() . ".jpg";
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error=1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////

        $precio = $_POST["precio"];
        $descripcionR = $_POST["descripcion_receta"];
        $idCategoria = $_POST["idCategoria"];
        $idRegion = $_POST["idRegion"];
        $idComplejidad = $_POST["idComplejidad"];

        


        if ($error == 0) //Si no hay error en la foto, pasa:
        {
            //Envio esos datos que tendra el plato modificado:
            $plato = new Plato ($_GET["idP"], $nombre, $descripcion, $urlServidor, $precio, $descripcionR, $idCategoria, $idRegion, $idComplejidad);
            $plato -> editar();

            //Ahora se le actualiza ese plato a la carta seleccionada, es decir la relacion:

            //Para saber el id de la carta que se inserto por ultima vez, que es 
            //el que neceistamos guardar en la relación:
            $idPlato = $_GET["idP"];
            $idCarta = $_POST["idCarta"];

            //Ahora si tenemos todos los datos para relacionar:
            $relacion = new Plato_Carta ($idPlato, $idCarta);
            $relacion -> editar ();
        }     
    }
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Editando Plato
                            </font>
                        </h1>
                    </div>
                </div>
            </div>

            <br><br><br><br>

            <div class="container wow fadeInUp">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><?php echo $p -> getNombre(); ?></h3>
                    </div>
                    <!-- Para avisar cuando se edita el plato -->
                    <?php
                        if(isset($_POST["editar"]) && $error == 0)
                        { 
                    ?>
                    <section id="alert2">
                        <div class="alerta">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Genial: El plato ha sido actualizada con éxito!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>

                    <!-- Para reporatar error en el nombre de la carta-->
                    <?php
                        if (isset($_POST["editar"]) && $error == 1)
                        { 
                    ?>
                    <section id="alert1">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> Error: El archivo foto debe ser de tipo
                                (jpg o png)</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }               
                    ?>
                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/editarPlato.php") . "&m&idP=" . $_GET["idP"] . "&idCar=" . $_GET["idCar"] . "&reg=" . $_GET["reg"] . ""?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="nombre" class="form-control" placeholder="Escribe el nuevo nombre" value ="<?php echo $p -> getNombre(); ?>"
                                    required="required">
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="descripcion" class="form-control"
                                    placeholder="Escribe la descripcion del plato" value= "<?php echo $p -> getDescripcion(); ?>" required="required">
                            </div>

                            <div class="form-group col-sm-4">
                                <input type="number" name="precio" class="form-control" placeholder="Escribe el nuevo precio" value="<?php echo $p -> getPrecio(); ?>"
                                    required="required">
                            </div>
                        </div>

                        <br>

                        <div class="form-row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="descripcion_receta" class="form-control"
                                    placeholder="Escribe la descripción de la receta" value="<?php echo $p -> getDescripcion_receta(); ?>" required="required">
                            </div>

                            <div class="form-group col-sm-4">
                                <select id="inputState" class="form-control" name="idComplejidad">
                                    <option value="-1">Nivel de Complejidad</option>
                                    <option value="1">Bajo</option>
                                    <option value="2">Basico</option>
                                    <option value="3">Medio</option>
                                    <option value="4">Alto</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <select id="inputState" class="form-control" name="idCategoria">
                                    <option value="-1">Categoria</option>
                                    <option value="1">Sopas</option>
                                    <option value="2">Carnes</option>
                                    <option value="3">Pollo</option>
                                    <option value="4">Postres</option>
                                    <option value="5">Arroces</option>
                                    <option value="6">Pescados</option>
                                </select>
                            </div>
                        </div>

                        <br>

                        <div class="form-row">
                            <div class="form-group col-sm-3">
                                <select id="inputState" class="form-control" name="idRegion">
                                    <option value="-1">Region</option>
                                    <option value="1">Andina</option>
                                    <option value="2">Amazonia</option>
                                    <option value="3">Caribe</option>
                                    <option value="4">Llanos Orientales</option>
                                    <option value="5">Pacifica</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-5">
                                <div class="form-row">
                                    <div class="form-group col-sm-2">
                                        <label style="font-family: arial black;">Foto:</label>
                                    </div>

                                    <div class="form-group col-sm-10">
                                        <div class="form-group">
                                            <input type="file" name="foto" class="form-control-file"
                                                required="required">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-4">
                                <div class="form-row">
                                    <div class="form-group col-sm-3">
                                        <label style="font-family: arial black;">Incluir en Carta:</label>
                                    </div>

                                    <div class="form-group col-sm-9">
                                        <select id="inputState" class="form-control" name="idCarta">
                                            <?php 
                                                //Consulto las cartas existentes para que se pueda elegir
                                                //a cual desea inlcuir el plato:
                                                $carta = new Carta();
                                                $cartas = $carta -> consultarTodos(); 

                                                foreach ($cartas as $c)
                                                {
                                                    echo "<option value='" . $c->getIdCarta() . "'>" . $c->getNombre() . "</option>";
                                                }       
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <input type="submit" name="editar" value="Guardar" class="btn login_btn btn-block">
                        </div>

                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&idCar=<?php echo $_GET["idCar"];?>&reg=<?php echo $_GET["reg"];?>">
                                Atras</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>