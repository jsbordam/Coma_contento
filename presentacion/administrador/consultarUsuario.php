<?php
if (isset($_SESSION["id"]))
{
    if ($_SESSION["rol"] == "Administrador") //Funcion solo para admi
    {
?>
<?php   
    //Consulto la lista de todos los usuarios:
    $usuario = new Usuario();
    $usuarios = $usuario -> consultarTodos();
?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Usuarios registrados en el sistema</span>
                        </h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>

                <table class="table table-warning table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Nombre</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Apellido</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Correo</font>
                                </h5>
                            </th>
							<th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Rol</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Estado</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Foto</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='3' color='black'>Acciones</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
						foreach ($usuarios as $usuarioActual)
						{                  	   
							echo "<tr>";
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getIdUsuario() . "</font></h5></td>";             	        
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getNombre() . "</font></h5></td>";
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getApellido() . "</font></h5></td>";
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $usuarioActual->getCorreo() . "</font></h5></td>";
								
								//Para rol:
								if ($usuarioActual->getIdRol() == 1)
								{
									$rol = "Admin";
								}
								else if ($usuarioActual->getIdRol() == 2)
								{
									$rol = "Cliente";
								}
								else if ($usuarioActual->getIdRol() == 3)
								{
									$rol = "Chef";
								}
								else 
								{
									$rol = "Mesero";
								}
								echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $rol . "</font></h5></td>";
								echo "<td class='text-center'><h5><font face='Arial' size='5' color='black'><div id='estado" . $usuarioActual -> getIdUsuario() . "'>" . (($usuarioActual -> getEstado() == 1)?"<i class='fas fa-user-check' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i>":"<i class='fas fa-user-times' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i>") . "</font></h5></td>";
								echo "<td class='text-center'><a href='modalFoto.php?idUsu=". $usuarioActual -> getIdUsuario() ."' data-toggle='modal' data-target='#modalFoto'><font face='Arial' size='5' color='red'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver Foto'></i></font></a></td>"; 
								echo "<td class='text-center'><a href='#'><font size='5' color='red'><i id='cambiarEstado" . $usuarioActual -> getIdUsuario() . "' class='fas fa-user-edit' data-toggle='tooltip' data-placement='bottom' title='Cambiar Estado'></i></font></a></td>";
							echo "</tr>";                	   
						}              	
            		?>
                    </tbody>
                </table>
                <br>
                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarUsuario.php")?>&m"
                                    class='btn float-right login_btn3'>
                                    ACTUALIZAR
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>


<!--Este es el Modal de Ver Foto: -->

<section id="modalF">
    <div class="modal fade" id="modalFoto" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content"></div>
        </div>
    </div>
</section>

<!--Este JavaScript es para el Modal de Acciones: -->
<script>
$('body').on('show.bs.modal', '.modal', function(e) {
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
</script>


<!--Este es el JQuery para cambiar estado: -->
<script>
    $(document).ready(function()
    {
        <?php 
            foreach ($usuarios as $usuarioActual)
            {
                echo "$(\"#cambiarEstado". $usuarioActual -> getIdUsuario() ."\").click(function()\n";
                echo "{\n";
                //Definimos la ruta en donde se recargara esta capa y la encriptamos respectivamente:
                echo "    url = \"indexAjax.php?pid=" . base64_encode("presentacion/administrador/cambiarEstadoUsuarioAjax.php") . "&idUsu=" . $usuarioActual -> getIdUsuario() . "\"\n";
                echo "    $(\"#estado" . $usuarioActual -> getIdUsuario() . "\").load(url);\n";
                echo "});\n\n";
            }                  
        ?> 
    });
</script>

<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>" />
<?php
}
?>