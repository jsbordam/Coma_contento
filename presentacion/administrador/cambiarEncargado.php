<?php 
    $r = new Region ($_GET["idReg"]);
    $r -> consultar ();

    //Para editar region:
    if (isset($_POST["editar"]))
    {
        $idUsuario = $_POST["idUsuario"];

        //Se cambia el encargado de la region:
        $reg = new Region ($_GET["idReg"], "", $idUsuario);
        $reg -> editar ();
   
    }
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Cambiando encargado
                            </font>
                        </h1>
                    </div>
                </div>
            </div>

            <br><br><br><br>

            <div class="container wow fadeInUp">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Región <?php echo $r -> getNombre();?></h3>
                    </div>
                    <br>
                    <!-- Para avisar cuando se edita la region -->
                    <?php
                            if(isset($_POST["editar"]))
                            { 
                    ?>
                    <section id="alert2">
                        <div class="alerta">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Genial: El encargado de la región se ha
                                    cambiado con éxito!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>

                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/cambiarEncargado.php") . "&m&idReg=" . $_GET["idReg"] . "&idCar=" . $_GET["idCar"] . ""?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-3">
                                <label style="font-family: arial black;"><font size="5">Lista de Chef's:</font></label>
                            </div>

                            <div class="form-group col-sm-9">
                                <select id="inputState" class="form-control" name="idUsuario">
                                    <?php 
                                                //Consulto las cartas existentes para que se pueda elegir
                                                //a cual desea inlcuir el plato:
                                                $usuario = new Usuario("", "", "", "", "", "", "", "", "", 3);
                                                $usuarios = $usuario -> consultarTodos(); 

                                                foreach  ($usuarios as $u)
                                                {
                                                    echo "<option value='" . $u->getIdUsuario() . "'>" . $u->getNombre() . "</option>"; 
                                                }       
                                            ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <input type="submit" name="editar" value="Guardar" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&idCar=<?php echo $_GET["idCar"];?>&reg=<?php echo $_GET["idReg"];?>">
                                Atras</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>