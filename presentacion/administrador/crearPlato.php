<?php 
    //Para crear plato:
    if (isset($_POST["crearP"]))
    {
        $error=0;
        //Estos son los datos que llegan por el formulario cuando se quiere crear:
        $nombre = $_POST["nombre"];
        $descripcion = $_POST["descripcion"];


        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
            
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "img/platos/" . time() . ".jpg";
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error=1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////

        $precio = $_POST["precio"];
        $descripcionR = $_POST["descripcion_receta"];
        $idCategoria = $_POST["idCategoria"];
        $idRegion = $_POST["idRegion"];
        $idComplejidad = $_POST["idComplejidad"];


        if ($error == 0) //Si no hay error en la foto pasa:
        {
            //Envio esos datos que tendra la carta:
            $plato = new Plato ("", $nombre, $descripcion, $urlServidor, $precio, $descripcionR, $idCategoria, $idRegion, $idComplejidad);
            $plato -> crear();

            //Capturo el id de ese plato que se acaba de crear:
            $plato -> ultimoId();
            $idPlato = $plato -> getIdPlato();

            //Ahora se le agrega ese plato a la carta creada, es decir la relacion:

            //Para saber el id de la carta que se inserto por ultima vez, que es 
            //el que neceistamos guardar en la relación:
            $idCarta = $_POST["idCarta"];

            //Ahora si tenemos todos los datos para relacionar:
            $relacion = new Plato_Carta ($idPlato, $idCarta);
            $relacion -> crear ();
        }     
    }
?>



<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <?php 
                    if (isset($_GET["p"])) //Quiere solo crear plato
                    {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Creando Platos
                            </font>
                        </h1>
                    </div>
                </div>
                <?php 
                    }
                    else
                    {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Creando Nuevo Menú
                            </font>
                        </h1>
                    </div>
                </div>
                <?php 
                    }
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <h1 align="left">
                            <font face="Arial Black" size="4">
                                PARAMETROS A TENER EN CUENTA:
                            </font>
                        </h1>
                    </div>
                </div>

                <div class="row-mt">
                    <div class="col-md-12">
                        <p align="left">
                            <font face="Arial Black" size="3">
                                1. Crear una carta<br>
                            </font>
                            <mark style="background-color: #ff0000;">
                                <font face="Arial Black" size="3">
                                    2. Crear platos y agregarlos a la carta<br>
                                </font>
                            </mark>
                            <font face="Arial Black" size="3">
                                3. Crear ingredientes que son agregados al plato
                            </font>
                        </p>
                    </div>
                </div>
            </div>

            <br><br><br><br>


            <div class="container wow fadeInUp">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">PLATO</h3>
                        <br>
                    </div>
                    <!-- Para avisar que debe ingresar ingredientes de ese plato -->
                    <?php
                        if(isset($_GET["n"]))
                        { 
                    ?>
                    <section id="alert3">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-edit"></i> Ahora ingresa los platos que tendra la carta</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>


                    <!-- Para avisar cuando se crea el plato -->
                    <?php
                            if(isset($_POST["crearP"]) && $error == 0)
                            { 
                    ?>
                    <section id="alert2">
                        <div class="alerta">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Genial: Plato creado con éxito!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>


                    <!-- Para reportar error en el archivo de foto-->
                    <?php
                        if (isset($_POST["crearP"]) && $error == 1)
                        { 
                    ?>
                    <section id="alert1">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> Error: El archivo foto debe ser de tipo
                                (jpg o png)</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }               
                    ?>


                    <!-- Para avisar cuando termina de agregar ingredientes -->
                    <?php
                        if(isset($_GET["terminado"]))
                        { 
                    ?>
                    <section id="alert2">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-check-circle"></i> Ingredientes guardados con éxito!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                        }
                    ?>
                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/crearPlato.php") . "&m"?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="nombre" class="form-control" placeholder="Nombre"
                                    required="required">
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="descripcion" class="form-control"
                                    placeholder="Descripción del plato" required="required">
                            </div>

                            <div class="form-group col-sm-4">
                                <input type="number" name="precio" class="form-control" placeholder="Precio al publico"
                                    required="required">
                            </div>
                        </div>

                        <br>

                        <div class="form-row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="descripcion_receta" class="form-control"
                                    placeholder="Descripción de la receta" required="required">
                            </div>

                            <div class="form-group col-sm-4">
                                <select id="inputState" class="form-control" name="idComplejidad">
                                    <option value="-1">Nivel de Complejidad</option>
                                    <option value="1">Bajo</option>
                                    <option value="2">Basico</option>
                                    <option value="3">Medio</option>
                                    <option value="4">Alto</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-4">
                                <select id="inputState" class="form-control" name="idCategoria">
                                    <option value="-1">Categoria</option>
                                    <option value="1">Sopas</option>
                                    <option value="2">Carnes</option>
                                    <option value="3">Pollo</option>
                                    <option value="4">Postres</option>
                                    <option value="5">Arroces</option>
                                    <option value="6">Pescados</option>
                                </select>
                            </div>
                        </div>

                        <br>

                        <div class="form-row">
                            <div class="form-group col-sm-3">
                                <select id="inputState" class="form-control" name="idRegion">
                                    <option value="-1">Region</option>
                                    <option value="1">Andina</option>
                                    <option value="2">Amazonia</option>
                                    <option value="3">Caribe</option>
                                    <option value="4">Llanos Orientales</option>
                                    <option value="5">Pacifica</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-5">
                                <div class="form-row">
                                    <div class="form-group col-sm-2">
                                        <label style="font-family: arial black;">Foto:</label>
                                    </div>

                                    <div class="form-group col-sm-10">
                                        <div class="form-group">
                                            <input type="file" name="foto" class="form-control-file"
                                                required="required">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-4">
                                <div class="form-row">
                                    <div class="form-group col-sm-3">
                                        <label style="font-family: arial black;">Incluir en Carta:</label>
                                    </div>

                                    <div class="form-group col-sm-9">
                                        <select id="inputState" class="form-control" name="idCarta">
                                            <?php 
                                                //Consulto las cartas existentes para que se pueda elegir
                                                //a cual desea inlcuir el plato:
                                                $carta = new Carta();
                                                $cartas = $carta -> consultarTodos(); 

                                                foreach ($cartas as $c)
                                                {
                                                    echo "<option value='" . $c->getIdCarta() . "'>" . $c->getNombre() . "</option>";
                                                }       
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <input type="submit" name="crearP" value="Crear" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/administrador/crearCarta.php")?>&m">
                                Crear nueva carta</a>
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/administrador/sesionAdministrador.php")?>&m">
                                Salir</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>

<?php
    //Si crea un plato lo redirige a los ingredientes:
    if(isset($_POST["crearP"]) && $error == 0)
    { 
?>
<meta http-equiv="refresh"
    content="4;url=index.php?pid=<?php echo base64_encode("presentacion/administrador/crearIngre.php")?>&m&n=<?php echo $nombre;?>&idPlato=<?php echo $idPlato;?>" />
<?php 
    }
?>