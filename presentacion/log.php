<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
				<div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">LOG <?php echo $_SESSION["rol"]?></span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <div class="card4">
                            <div class="row">
                                <div class="col-md-3" align="right">
                                    <div class="card-header">
                                        <font face="Arial Black" size="4">
                                            <i class="fas fa-search"></i> Buscar
                                        </font>
                                    </div>
                                </div>

                                <div class="col-md-9" align="left">
                                    <div class="card-header">
                                        <div class="form-group">
                                            <input type="text" id="filtro" class="form-control" placeholder="Filtro">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <br><br>

                <div class="row mt-3">
                    <div class="col">
                        <div id="resultados"></div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>

<!--Este es el JQuery para buscar LOG: -->
<script>
$(document).ready(function() {
    $("#filtro").keyup(function() {
        if ($("#filtro").val().length > 0) {
            //Encriptamos la ruta para que no sea visible en la inspección de codigo:
            url =
                "indexAjax.php?pid=<?php echo base64_encode("presentacion/buscarLogAjax.php") ?>&filtro=" +
                $("#filtro").val();
            $("#resultados").load(url);
        }
    });
});
</script>