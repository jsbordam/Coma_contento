<?php
    $filtro = $_GET["filtro"];
    
    //Traigo el arreglo de registros del LOG asociados a ese filtro:
    $log = new Log("", "", "", "", "", "", "", $_SESSION["id"]);
    $arregloLog = $log -> buscar ($filtro);
?>



<table class="table table-warning table-striped table-hover">
	<thead>
		<tr>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Codigo</font>
				</h5></th>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Accion</font>
				</h5></th>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Datos</font>
				</h5></th>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Fecha</font>
				</h5></th>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Hora</font>
				</h5></th>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Ip</font>
				</h5></th>
			<th class="text-center"><h5>
					<font face='Arial Black' size='4' color='black'>Sistema Operativo</font>
				</h5></th>
		</tr>
	</thead>
	<tbody>
        <?php       
            //Para resaltar el filtro que se esta buscando:
            
                //Convierte toda la cadena a minusculas
                $filtro = strtolower ($filtro);
                //Convierte la primera letra en Mayuscula
                $filtro = ucwords($filtro);
            
            foreach ($arregloLog as $logActual) 
            {           
                // Muestro los datos del log:
                echo "<tr>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $logActual->getIdLog() . "</font></h5></td>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . str_replace($filtro, "<font color='red'>" . $filtro . "</font>", $logActual->getAccion()) . "</font></h5></td>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $logActual->getDatos() . "</font></h5></td>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $logActual->getFecha() . "</font></h5></td>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $logActual->getHora() . "</font></h5></td>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $logActual->getIp() . "</font></h5></td>";
                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $logActual->getSo() . "</font></h5></td>";
                echo "</tr>";
            }
        ?>
	</tbody>
</table>