<?php
    //Primero consulto la carta que este activa en el restaurante:
    $c = new Carta ();
    $cartas = $c -> consultarTodos(); 

    foreach ($cartas as $cartaActual)
    {
        if ($cartaActual->getEstado() == 1)
        {
            $idCarta = $cartaActual -> getIdCarta();
        }
    }
?>
    <?php 
        if (isset($_GET["c"]))
        {
            echo "<div class=''>";
            echo "<main class='page-content'>";
        }
        else
        {
    ?>
<!-- Page-->
<div class="page text-center">
    <!-- Page Contents-->
    <main class="page-content">
        <!--Sección de encabezado de Carta: -->
        <section class="context-dark">
            <div class="parallax-container"
                data-parallax-img="http://joshuacafebar.com/web15/wp-content/uploads/2015/07/bg_parallax_5.jpg">
                <div class="parallax-content">
                    <div class="container">
                        <div class="section-110 section-cover row justify-content-sm-center align-items-sm-center">
                            <div class="col-lg-8">
                                <img src="https://rawcdn.githack.com/dubidun/Demo-1/5e54bf90ae847403be4090a02bd81c2f5a4d36b7/intro-logo-175x173.png"
                                    alt="" width="175" height="173">
                                <h1 class="font-accent"><span class="big">Carta</span></h1>
                                <ul class="list-inline list-inline-dashed p">
                                    <li class="list-inline-item">
                                        <?php
                                            if (isset($_SESSION["id"]))
                                            {                                
                                        ?>
                                        <a
                                            href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php") ?>&m">
                                            <font size="5px">Inicio</font>
                                        </a>
                                        <?php 
                                            }
                                            else
                                            {
                                        ?>
                                        <a
                                            href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php") ?>&m">
                                            <font size="5px">Inicio</font>
                                        </a>
                                        <?php 
                                            }
                                        ?>
                                    </li>
                                    <li class="list-inline-item">
                                        <font size="5px">Carta</font>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </section>
        <?php 
            }
        ?>

        <!-- Section About us-->
        <section class="section-66 section-lg-110">
            <section id="portfolio"
                style="background-image: url(https://s1.1zoom.me/big0/263/Colombia_Scenery_Rivers_Coast_Houses_Clouds_Hill_529353_1280x850.jpg)">
                <div class="container wow fadeInUp">
                    <div class="row justify-content-xl-center">
                        <div class="col-xl-12">
                            <h1 class="text-center"><span class="d-block font-accent big">Nuestra Carta</span></h1>
                            <hr class="divider bg-mantis offset-top-30">
                        </div>
                    </div>
                </div>


                <br><br>

                <!-- Aqui empiezan a mostrarse las regiones:  -->
                <div class="container wow fadeInUp">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card2">
                                <a class="portfolio-item"
                                    style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-4-728.jpg?cb=1315044688);"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=2&idCar=<?php echo $idCarta;?>">
                                    <div class="details">
                                        <h3 class='card-title'>
                                            <font size="6">CCC Restaurant</font>
                                        </h3>
                                        <br>
                                        <h2 class="card-text">
                                            <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                        </h2>
                                    </div>
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <font>Amazonía</font>
                                    </h4>
                                    <h3 class="card-text">
                                        <font face="Arial">
                                            <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 2);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                        </font>
                                    </h3>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="card2">
                                <a class="portfolio-item"
                                    style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-3-728.jpg?cb=1315044688);"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=1&idCar=<?php echo $idCarta;?>">
                                    <div class="details">
                                        <h3 class='card-title'>
                                            <font size="6">CCC Restaurant</font>
                                        </h3>
                                        <br>
                                        <h2 class="card-text">
                                            <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                        </h2>
                                    </div>
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <font>Andina</font>
                                    </h4>
                                    <h3 class="card-text">
                                        <font face="Arial">
                                            <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 1);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                        </font>
                                    </h3>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="card2">
                                <a class="portfolio-item"
                                    style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-1-728.jpg?cb=1315044688);"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=3&idCar=<?php echo $idCarta;?>">
                                    <div class="details">
                                        <h3 class='card-title'>
                                            <font size="6">CCC Restaurant</font>
                                        </h3>
                                        <br>
                                        <h2 class="card-text">
                                            <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                        </h2>
                                    </div>
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <font>Caribe</font>
                                    </h4>
                                    <h3 class="card-text">
                                        <font face="Arial">
                                            <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 3);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                        </font>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="card2">
                                <a class="portfolio-item"
                                    style="background-image: url(https://image.slidesharecdn.com/comidatipicaregionescolombia-110903101025-phpapp02/95/comida-tipica-regiones-de-colombia-5-728.jpg?cb=1315044688);"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=4&idCar=<?php echo $idCarta;?>">
                                    <div class="details">
                                        <h3 class='card-title'>
                                            <font size="6">CCC Restaurant</font>
                                        </h3>
                                        <br>
                                        <h2 class="card-text">
                                            <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                        </h2>
                                    </div>
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <font>Llanos Orientales</font>
                                    </h4>
                                    <h3 class="card-text">
                                        <font face="Arial">
                                            <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 4);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                        </font>
                                    </h3>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="card2">
                                <a class="portfolio-item"
                                    style="background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzUMrwcQ_S4zQr4rBG9ATDu9kk_ZakuerZMg&usqp=CAU);"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&reg=5&idCar=<?php echo $idCarta;?>">
                                    <div class="details">
                                        <h3 class='card-title'>
                                            <font size="6">CCC Restaurant</font>
                                        </h3>
                                        <br>
                                        <h2 class="card-text">
                                            <font face="Arial Black"><i class="fas fa-search"></i> Ver mas</font>
                                        </h2>
                                    </div>
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <font>Pacifica</font>
                                    </h4>
                                    <h3 class="card-text">
                                        <font face="Arial">
                                            <?php 
                                            //Para numero de productos en la categoria:
                                            $plato = new Plato ("", "", "", "", "", "", "", 5);
                                            echo "(" . $plato -> contarPla($idCarta) . ")";
                                        ?>
                                        </font>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>


                    <br><br>

                    <?php 
                        if ($_SESSION["rol"] == "Administrador")
                        {
                    ?>
                    <div class="row justify-content-center grid-group-md text-xl-center">
                        <div class="row">
                            <div class="col-md-12 col-xl-12">
                                <div class="group offset-top-50">
                                    <a class="btn btn-lg btn-primary"
                                        href="index.php?pid=<?php echo base64_encode("presentacion/administrador/gestionarCarta.php")?>&m">Atras</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        }
                        else
                        {
                    ?>
                    <div class="row justify-content-center grid-group-md text-xl-center">
                        <div class="row">
                            <div class="col-md-12 col-xl-12">
                                <div class="group offset-top-50">
                                    <a class="btn btn-lg btn-primary"
                                        href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php")?>&m">Atras</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        }
                    ?>
            </section>
        </section>
    </main>



    <!-- Page Footer--><a class="banner" href="#" target="_blank"><img
            src="https://irp-cdn.multiscreensite.com/c1e0f858/MOBILE/jpg/1300863-restaurant-don-humberto-banner-2.jpg"
            alt=""></a>

    <!-- Default footer-->
    <footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark">
        <div class="container">
            <div class="row justify-content-md-center text-xl-left">
                <div class="col-md-12">
                    <div class="row justify-content-sm-center">
                        <div
                            class="col-sm-10 col-md-6 text-sm-left col-lg-6 col-xl-4 order-xl-3 order-lg-2 offset-xxl-1">
                            <h6 class="text-uppercase text-spacing-60 text-center text-lg-left">Contacto</h6>
                            <!-- RD Mailform -->
                            <form class="rd-mailform offset-top-34 offset-lg-top-0 text-left"
                                data-form-output="form-output-global" data-form-type="contact" method="post"
                                action="bat/rd-mailform.php">
                                <div class="form-group">
                                    <label class="form-label form-label-sm" for="footer-v2-name">Tú Nombre:</label>
                                    <input class="form-control input-sm form-control-impressed" id="footer-v2-name"
                                        type="text" name="name" data-constraints="@Required">
                                </div>
                                <div class="form-group offset-top-20">
                                    <label class="form-label form-label-sm" for="footer-v2-email">Tú Email*:</label>
                                    <input class="form-control input-sm form-control-impressed" id="footer-v2-email"
                                        type="text" name="email" data-constraints="@Email @Required">
                                </div>
                                <div class="form-group offset-top-20">
                                    <label class="form-label form-label-sm" for="footer-v2-message">Mensaje*:</label>
                                    <textarea class="form-control input-sm form-control-impressed"
                                        id="footer-v2-message" name="message" data-constraints="@Required"
                                        style="height: 80px"></textarea>
                                </div>
                                <div class="group offset-top-20">
                                    <button class="btn btn-sm btn-primary" type="submit"
                                        style="padding-left: 30px; padding-right: 30px;">Enviar</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 offset-top-66 col-xl-3 order-xl-1 offset-xl-top-0 order-lg-3">
                            <!-- Footer brand-->
                            <div class="footer-brand"><a href="index.php">
                                    <font face="Arial black" size="6" color="white"><i class="fas fa-utensils"></i>
                                        CCC
                                    </font>
                                    <font face="Brush Script MT" size="6" color="white">Restaurant</font>
                                </a></div>
                            <p class="text-darker offset-top-4">¡Obtén la mejor experiencia gastronómica!</p>
                            <address class="contact-info offset-top-30 p">
                                <div>
                                    <dl>
                                        <dt class="text-white">Visitanos en:</dt>
                                        <dd class="text-dark d-xl-block">Cra 196 #67 - 34 <span
                                                class="d-xl-block">Bogota, Colombia</span> <span class="d-xl-block">IL
                                                60604-2340.</span></dd>
                                    </dl>
                                </div>
                                <div>
                                    <dl class="offset-top-0">
                                        <dt class="text-white">Telefono:</dt>
                                        <dd class="text-dark"><a href="tel:#">+57 3013225781</a></dd>
                                    </dl>
                                </div>
                                <div>
                                    <dl class="offset-top-0">
                                        <dt class="text-white">E-mail:</dt>
                                        <dd class="text-dark"><a href="mailto:#">cccrestaurant@gmail.com</a></dd>
                                    </dl>
                                </div>
                            </address>
                        </div>
                        <div
                            class="col-sm-10 col-md-6 offset-top-66 offset-lg-top-0 text-sm-left col-lg-6 col-xl-4 order-xl-2 order-lg-1">
                            <h6 class="text-uppercase text-spacing-60 text-center text-lg-left">LO QUE DICE LA GENTE
                            </h6>
                            <!-- Simple quote Slider-->
                            <div class="owl-carousel owl-carousel-classic owl-carousel-class-light owl-carousel-simple-quote veil-owl-nav"
                                data-items="1" data-nav="false" data-dots="true">
                                <div>
                                    <blockquote class="quote quote-simple-2 text-left">
                                        <p class="quote-body offset-bottom-0">
                                            <q>Este es uno de los mejores restaurantes que he visitado. Todo estuvo
                                                excelente. La comida se sirvió de una manera muy creativa. Se lo
                                                recomendaría a todos.</q>
                                        </p>
                                        <h6 class="quote-author text-uppercase text-white">
                                            <cite class="text-normal">- Leonardo DiCaprio</cite>
                                        </h6>
                                    </blockquote>
                                </div>
                                <div>
                                    <blockquote class="quote quote-simple-2 text-left">
                                        <p class="quote-body offset-bottom-0">
                                            <q>Su restaurante es el mejor lugar familiar para mí. Ya es una
                                                tradición
                                                para nosotros visitar este lugar todos los viernes por la noche.
                                                Realmente disfruto de todo.</q>
                                        </p>
                                        <h6 class="quote-author text-uppercase text-white">
                                            <cite class="text-normal">- Emma Watson</cite>
                                        </h6>
                                    </blockquote>
                                </div>
                                <div>
                                    <blockquote class="quote quote-simple-2 text-left">
                                        <p class="quote-body offset-bottom-0">
                                            <q>Aquí es donde voy cada vez que me siento abrumado por la nostalgia o
                                                simplemente quiero probar la verdadera pasta una vez más. Altamente
                                                recomendado</q>
                                        </p>
                                        <h6 class="quote-author text-uppercase text-white">
                                            <cite class="text-normal">- Zac Efron</cite>
                                        </h6>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="offset-top-34 text-sm-center text-lg-left">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <font size="7"><a href="#"><i class="fab fa-facebook"></i></a></font>
                                    </li>
                                    <li class="list-inline-item">
                                        <font size="7"><a href="#"><i class="fab fa-twitter-square"></i></a></font>
                                    </li>
                                    <li class="list-inline-item">
                                        <font size="7"><a href="#"><i class="fab fa-google-plus"></i></a></font>
                                    </li>
                                    <li class="list-inline-item">
                                        <font size="7"><a href="#"><i class="fab fa-youtube-square"></i></a></font>
                                    </li>
                                    <li class="list-inline-item">
                                        <font size="7"><a href="#"><i class="fab fa-instagram"></i></a></font>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </footer>
</div>