<?php 
    $error = 0;
    //Para registro de Usuarios:
    //(Recopilo los datos que llegan por el formulario)
    
    if (isset($_POST["crearCuenta"]))
    {
        //Recojo los datos que llegan por POST del formulario:
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        
        //Para opcion tipo_docto:
        if ($_POST["tipo_docto"] == "Cedula Ciudadania")
        {
            $tipo_docto = 0;
        }
        else if ($_POST["tipo_docto"] == "Tarjeta de Identidad")
        {
            $tipo_docto = 1;
        }
        else
        {
            $tipo_docto = 2; //Carnet de Extranjeria
        }
        
        $num_docto = $_POST["num_docto"];
       
        $correo = $_POST["correo"];
        $clave = md5 ($_POST["clave"]); //Encriptamos la clave con MD5
        $estado = 1; //Habilitado
        
        //////////////////////////////////////////////////////////////////////////////////
        //Para el archivo de foto:
        
        $foto = $_FILES["foto"];
        $tipo = $foto ["type"];
        
        if ($tipo == "image/jpeg" || $tipo == "image/png")
        {
        
            //Esta sera la direccion final que tendra la foto para que se guarde en la carpeta
            //del proyecto:
            
            //Se le da nombre con time() para que guarde la fecha y la hora excata en que cambia su foto
            //De esta manera se controla que no hayan fotos con el mismo nombre:
            
            $urlServidor = "img/usuarios/" . time() . ".jpg"; 
            $urlLocal = $foto ["tmp_name"];// tmp_name es el nombre temporal que tiene ese archivo en el htdocs
            
            copy($urlLocal, $urlServidor); //Copie la imagen de local a servidor
        }
        else
        {
            $error = 1;
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        
        //Para el rol con el que se registra:
        if ($_POST["idRol"] == "Cliente")
        {
            $idRol = 2;
        }
        else
        {
            $idRol = 3; //Rol de proveedor
        }
        
        
        if ($error == 0)
        {
            //Creo el usuario:
            $usuario = new Usuario ("", $nombre, $apellido, $tipo_docto, $num_docto, $correo, $clave, $estado, $urlServidor, $idRol);
            $usuario -> crear();
        }     
    }     
?>
<br><br><br><br><br><br><br>
<section id="register">
    <div class="d-flex justify-content-center h-100">
        <div class="card2">
            <div class="card-header">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big"><font color="white">Te estas registrando en CCC Restaurant<font></span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="card-body">
                <!-- Para avisar cuando un usuario se registro en el sistema -->
                <?php
					if(isset($_GET["registrar"]) && $error == 0)
					{ 
				?>
                <section id="alert2">
                    <div class="alert alert-dismissible fade show" role="alert">
                        <strong><i class="fas fa-check-circle"></i> Usuario registrado correctamente. Ya puedes iniciar
                            sesion</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </section>
                <?php 
					}
				?>

                <!-- Para reporatar error en el archivo de foto-->
                <?php
					if (isset($_POST["crearCuenta"]) && $error == 1)
					{ 
				?>
                <section id="alert2">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error. El archivo foto debe ser de tipo: (jpg o png)</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </section>
                <?php 
					}               
				?>

                <form
                    action=<?php echo "index.php?pid=" . base64_encode("presentacion/menuPrincipal/registrarse.php") . "&registrar"?>
                    method="post" enctype="multipart/form-data">

                    <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label>Nombre:</label> <input type="text" name="nombre" class="form-control"
                                placeholder="Escribe tu nombre" required="required">
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Apellido:</label> <input type="text" name="apellido" class="form-control"
                                placeholder="Escribe tu apellido" required="required">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="inputState">Tipo Documento:</label> <select id="inputState" class="form-control"
                                name="tipo_docto">
                                <option>Cedula de ciudadania</option>
                                <option>Tarjeta de identidad</option>
                                <option>Carnet de extranjeria</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label>Num Documento:</label> <input type="number" name="num_docto" class="form-control"
                                placeholder="Escribe tu documento" required="required">
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Correo:</label> <input type="email" name="correo" class="form-control"
                                placeholder="Escribe tu correo" required="required">
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Clave:</label> <input type="password" name="clave" class="form-control"
                                placeholder="Escribe tu clave" required="required">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label for="inputState">Estado:</label> <select id="inputState" class="form-control"
                                name="estado">
                                <option>Habilitado</option>
                            </select>
                        </div>

                        <div class="form-group col-sm-4">
                            <label>Foto:</label>
                            <div class="form-group">
                                <input type="file" name="foto" class="form-control-file" required="required">
                            </div>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="inputState">Rol:</label> <select id="inputState" class="form-control"
                                name="idRol">
                                <option>Cliente</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <input type="submit" name="crearCuenta" value="Crear cuenta"
                            class="btn float-right login_btn btn-block">
                    </div>
                    <div class="form-group">
                        <a class="btn float-right login_btn3"
                            href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>">
                            VOLVER AL INICIO</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php
    //Para redireccionar a iniciar sesion cuando ya se haya registrado
    if(isset($_GET["registrar"]) && $error == 0)
    { 
?>
<meta http-equiv="refresh"
    content="4;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>" />
<?php 
    }
 ?>