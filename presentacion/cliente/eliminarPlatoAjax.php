<?php 
    //Cuando se quiere eliminar un plato de la bandeja:
    $bandeja = new Bandeja ("", $_GET["idPlato"]); //Envio el id del plato a eliminar
    $bandeja -> eliminar();

    
    //Despues busco la bandeja de platos que tiene registrado esa orden
    $bandeja = new Bandeja ($_GET["idOrden"]);
    
    //Me trae un array de todos los registros asociados a ese pedido
    $arregloBandeja = $bandeja -> consultarTodos(); 
    
    //Por ultimo muestro toda la info del carrito actualizado
?>


<table class="table table-hover">
    <?php 
        $valorTotal=0;
        foreach ($arregloBandeja as $registroActual)
        {      	    
            //Busco los datos del plato de cada registro:
            $plato = new Plato ($registroActual->getIdPlato());
            $plato -> consultar();
            
            $valorCantidad = $plato->getPrecio() * $registroActual->getCantidad_und();
            $valorTotal += $valorCantidad;
            
            //Muestro los datos del plato que necesito:
            echo "<tr>";
                echo "<td><img src='" . $plato->getFoto() . "' class='imgRedonda'></td>";  
                echo "<td>"; 
                echo "<font face='Arial Black' size='3' color='white'>" . $plato->getNombre() . "</font>";
                    echo "<br><font face='Arial Black' size='2' color='white'>-> $" . number_format($plato->getPrecio(), ...array(0, ',', '.')) . "</font>";
                    echo "<br><br><font face='Arial' size='3' color='white'>Cantidad x " . $registroActual->getCantidad_und() . "</font>";
                    echo "<b><br><font face='Arial' size='3' color='white'>$" . number_format($valorCantidad, ...array(0, ',', '.')) . "</font></b>";
                echo "</td>";       	        
                echo "<td><a href='#'><font size='5' color='red'><i id='eliminarProducto" . $registroActual->getIdPlato() . "' class='far fa-trash-alt'></i></font></a></td>";         	       
            echo "</tr>";   
        }       	
    ?>
</table>
<div class='row'>
    <div class='col-md-9'>
        <h4><font face='Arial' size='4' color='black'>Sub -Total</font></h4>           
    </div>
    <div class='col-md-3'>
        <h5><font face='Arial Black' size='4' color='black'>$<?php echo number_format($valorTotal, ...array(0, ',', '.'))?></font></h5>       
    </div>	
</div>
<div class='row'>
    <div class='col-md-9'>
        <h4><font face='Arial' size='4' color='black'>Total</font></h4>           
    </div>
    <div class='col-md-3'>
        <h5><font face='Arial Black' size='4' color='black'>$<?php echo number_format($valorTotal, ...array(0, ',', '.'))?></font></h5>       
    </div>	
</div>



<!--Este es el JQuery para eliminar un plato de la bandeja: -->
<script>
    $(document).ready(function()
    {
        <?php 
            foreach ($arregloBandeja as $registroActual)
            {
                echo "$(\"#eliminarProducto" . $registroActual->getIdPlato() . "\").click(function()\n";
                echo "{\n";
                //Actualiza numero de la bandeja:
                echo "    url2 = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/actualizarBandejaAjax.php") . "&idOrden=" . $registroActual->getIdOrden() . "\"\n";
                echo "    $(\"#actualizarC\").load(url2);\n";
                //Definimos la ruta en donde se recargara esta capa y la encriptamos respectivamente:
                echo "    url = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/eliminarPlatoAjax.php") . "&idPlato=" . $registroActual -> getIdPlato() . "&idOrden=" . $registroActual -> getIdOrden() . "\"\n";
                echo "    $(\"#actualizar\").load(url);\n";
                //Actualiza numero de la bandeja nuevamente:
                echo "    url2 = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/actualizarBandejaAjax.php") . "&idOrden=" . $registroActual->getIdOrden() . "\"\n";
                echo "    $(\"#actualizarC\").load(url2);\n";
                echo "});\n\n";
            }                  
        ?> 
    });
</script>