<?php 
    if (isset($_POST["reservar"]))
    {
        //Almacenamos los datos que llegan por POST del formulario:
        $restaurante = $_POST["restaurante"];
        $fecha = $_POST["fecha"];
        $hora = $_POST["hora"];
        $personas = $_POST["personas"];

        //Se reporta actividad N° 1 en el proceso:
    }
?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Reservas</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>

                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h4 class="text-center">
                            Para cumplir con las regulaciones del gobierno y por tu seguridad
                            debes reservar una mesa para el ingreso al restaurante.
                        </h4>
                    </div>
                </div>

                <br><br>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">DATOS</h3>
                    </div>
                    <!-- Para avisar cuando se crea la reserva -->
                    <?php
                            if(isset($_POST["reservar"]))
                            { 
                        ?>
                    <section id="alert2">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-check-circle"></i> Genial: Ya tienes una reserva, te
                                esperamos!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <br>
                    <?php 
                            }
                        ?>

                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/reservar.php") . "&m"?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Restaurante:</h4>
                                <select id="inputState" class="form-control" name="restaurante">
                                    <option value="1">Central 85</option>
                                    <option value="2">Central salitre</option>
                                    <option value="3">Los olivos</option>
                                    <option value="4">Villa luz</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Fecha: </h4>
                                <input type="date" name="fecha" class="form-control" required="required">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Hora:</h4>
                                <input type="time" name="hora" value="16:00:00" class="form-control" max="22:30:00"
                                    min="10:00:00" step="1" required="required">
                            </div>

                            <div class="form-group col-sm-6">
                                <h4 class="card-text">Personas:</h4>
                                <select id="inputState" class="form-control" name="personas">
                                    <option value="1">2</option>
                                    <option value="2">3</option>
                                    <option value="3">4</option>
                                    <option value="4">5</option>
                                </select>
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <input type="submit" name="reservar" value="Reservar" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn3"
                                href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php")?>&m">
                                Salir</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>