<?php
    $error = 0;
    //Primero obtengo el id del usuario que esta en la sesion:
    $idUsuario = $_SESSION["id"];
    
    //Luego busco el estado de la ultima orden de ese usuario.
    //Para ello, realizo una consulta en la tabla de orden:
    
    $orden = new Orden ("", "", "", "", $idUsuario);
    $orden -> ultimoId();
    $idOrden = $orden -> getIdOrden();

    //Luego consulto los datos de esa orden:
    $orden = new Orden ($idOrden);
    $orden -> consultarUno();

    //Ahora verifico el estado de la orden:
    //1 = Seleccionando
    //2 = Solicitada
    //3 = Atendida
    //4 = Preparada
    //5 = Tomada
    //6 = Entregada

    if ($orden -> getEstado() == 6 || $orden -> getEstado() == 0) 
    {
        //Si es 0:
        //Quiere decir que la orden fue cancelada, entonces se debe eliminar:

        $o = new Orden ($orden -> getIdOrden());
        $o -> eliminar();
     

        //Si es 6:
        //Quiere decir que su ultima orden ya fue entregada y ahora si puede crear otra

        //Capturo fecha actual:
        $fecha_hora = "";
        $valor_total = 0;
        $estado = 1; //Es el estado por defecto   (Seleccionando)     
        
        //Creo la orden con todos los atributos:
        $orden = new Orden ("", $fecha_hora, $valor_total, $estado, $idUsuario);
        $orden -> crear();
        $orden -> consultar(1); //Consulto de nuevo para actualizar las ordenes

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Ahora se agregan platos a la orden:
    
        $idOrden = $orden -> getIdOrden();
        $idPlato = $_GET["idPla"];
        
        $cantidad_und = 1; //Esta agregando 1 plato a la bandeja
        
        //Se verifica si quiere agregar un mismo plato en una misma orden:
        $bandeja = new Bandeja ($idOrden, $idPlato);
        
        
        if ($bandeja -> consultar())
        {
            //Actualizo la cantidad_und de esa orden, sumando la cantidad del 
            //plato que llega con la que tiene en la BD:
            
            $cantidad_und += $bandeja -> getCantidad_und();
            $bandeja = new Bandeja ($idOrden, $idPlato, $cantidad_und);
            $bandeja -> editar();
        }
        else 
        {
            //Creo el carrito con todos los atributos:
            $bandeja = new Bandeja ($idOrden, $idPlato, $cantidad_und);
            $bandeja -> crear();
        }
    }
    else if ($orden -> getEstado() == 1) //Ya tiene una orden activa y quiere agregar platos a esa
    {
        //Se agregan los platos a la orden ya existente:
    
        $idOrden = $orden -> getIdOrden();
        $idPlato = $_GET["idPla"];
        
        $cantidad_und = 1; //Esta agregando 1 plato a la bandeja
        
        //Se verifica si quiere agregar un mismo plato en una misma orden:
        $bandeja = new Bandeja ($idOrden, $idPlato);
        
        
        if ($bandeja -> consultar())
        {
            //Actualizo la cantidad_und de esa orden, sumando la cantidad del 
            //plato que llega con la que tiene en la BD:
            
            $cantidad_und += $bandeja -> getCantidad_und();
            $bandeja = new Bandeja ($idOrden, $idPlato, $cantidad_und);
            $bandeja -> editar();
        }
        else 
        {
            //Creo el carrito con todos los atributos:
            $bandeja = new Bandeja ($idOrden, $idPlato, $cantidad_und);
            $bandeja -> crear();
        }
    }
    else //Quiere decir que la orden fue solicitada pero aun no es entregada (estado 2 o 3)
    {
        //Entonces no le debe permitir crear otra orden, hasta que se le entregue esa ultima
        $error = 1;
    }
?>

<?php 
    if ($error == 1)
    {
        //Se reporta un error de que ya tiene un pedido solicitado y que no puede hacer más...
?>
<!-- Para redireccionar a los platos y lanzar aviso: -->
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&ordenNo&idCar=<?php echo $_GET["idCar"]?>&reg=<?php echo $_GET["reg"]?>"/>
<?php 
    }
    else
    {
?>
<!-- Para redireccionar a los platos y lanzar aviso: -->
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/platos/consultarPlatos.php")?>&m&agregado&idCar=<?php echo $_GET["idCar"]?>&reg=<?php echo $_GET["reg"]?>"/>
<?php 
    }
?>
