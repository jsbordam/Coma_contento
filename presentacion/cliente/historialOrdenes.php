<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Cliente") //Funcion solo para cliente
    {
?>
<?php
    //Primero busco la orden
    $orden = new Orden ("", "", "", "", $_GET["idUsuario"]);
    
    //Me trae un array de todos los registros asociados a esa orden
    $arregloOrdenes = $orden -> consultarTodos();
?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Historial de Ordenes</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>
                <table class="table table-warning table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Platos</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Estado</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                    foreach ($arregloOrdenes as $pedidoActual)
                	{   
                	    //Busco la bandeja de platos que tiene registrada esa orden
                	    $bandeja = new Bandeja ($pedidoActual->getIdOrden());
                	    
                	    //Me trae un array de todos los registros asociados a esa orden
                	    $arregloBandeja = $bandeja -> consultarTodos(); 
                	    
                	    $unidadesTotal=0;
                	    //Para calcular cuantos platos pidio en esa orden
                	    foreach ($arregloBandeja as $carritoActual)
                	    {
                	        $unidadesTotal += $carritoActual -> getCantidad_und();
                	    }
                	    
                	    //Muestro los datos de la orden que necesito:
                	    echo "<tr>";
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getIdOrden() . "</font></h5></td>";             	        
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $pedidoActual->getFecha_hora() . "</font></h5></td>";
                	       echo "<td class='text-center'><font face='Arial' size='3' color='black'>$" . number_format($pedidoActual->getValor_total(), ...array(0, ',', '.')) . "</font></td>";               	        
                	       echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                	       
                	       if ($pedidoActual->getEstado() == 1)//Seleccionando
                	       {
                	           echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SELECCIONANDO</font></h5></td>";
                	           echo "<td class='text-center'><h5><font face='Arial' size='5' color='black'><i class='fas fa-hourglass-half'></i></font></h5></td>";
                	       }
                           else if ($pedidoActual->getEstado() == 2)//Solicitada
                	       {
                                echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SOLICITADA</font></h5></td>";
                                echo "<td class='text-center'><h5><font face='Arial' size='5' color='black'><i class='fas fa-clipboard-check'></i></font></h5></td>";
                	       }
                	       else//Entregada 
                	       {
                	           echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>ENTREGADA</font></h5></td>";
                	           echo "<td class='text-center'><h5><font face='Arial' size='5' color='black'><i class='fas fa-utensils'></i></font></h5></td>";
                	       }               	       
                	   echo "</tr>";                	   
                	}              	
            	?>
                    </tbody>
                </table>
                <br>

                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/historialOrdenes.php")?>&m&idUsuario=<?php echo $_GET["idUsuario"]?>"
                                    class='btn float-right login_btn3'>
                                    ACTUALIZAR
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>
<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh"
    content="0;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>" />
<?php
}
?>