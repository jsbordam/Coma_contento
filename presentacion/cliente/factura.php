<?php 
if (isset($_SESSION["id"]))
{   
    if ($_SESSION["rol"] == "Cliente") //Funcion solo para cliente
    {
?>

<?php
    //Primero reviso cual fue la ultima orden ingresada del usuario porque esa es la se debe gestionar:
    $orden = new Orden ("", "", "", "", $_SESSION["id"]);
    $orden -> ultimoId();

    $idOrden = $orden->getIdOrden();

    //Sumar los platos que tiene en la bandeja:
    $bandeja = new Bandeja ($idOrden);
    $bandeja -> sumarProductos(); //Sumo los productos que tienen esa orden

    //Valido si tiene platos en la bandeja y de ser asi los elimino:
    if (isset($_GET["eliminar"])) //Cuando se quiere eliminar una orden
    { 
        
        if ($bandeja -> getCantidad_und() > 0)
        {
            $bandeja -> eliminar2(); //Elimino todos los productos asociados a esa orden
        }
        
        //Luego le cambio el estado a (0) de la orden que se quiere eliminar:
        $orden = new Orden ($idOrden);
        $orden -> editar2(0);
    }
    
    
    //Me trae un array de todos los registros asociados a esa orden
    $arregloBandeja = $bandeja -> consultarTodos(); 



    //Cuando se envian observaciones de una orden:
    if (isset($_POST["enviar"]))
    {
        $obs = $_POST["observaciones"];
        $obs = $obs . " - ";
        $obs = $obs . "Hora: " . date("h:i:s");

        //Luego las agrego a la orden:
        $o = new Orden ($idOrden);
        $o -> editar3 ($obs);
    }
?>


<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <?php 
                $orden = new Orden ($idOrden);
                $orden -> consultarUno();

                if ($orden -> getEstado() == 2 || $orden -> getEstado() == 3 || $orden -> getEstado() == 4 || $orden -> getEstado() == 5) //Si la orden fue solicitada
                {
            ?>

            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Detalles de tu Orden</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>

                <br><br>

                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-left">
                            <font size="6">La orden ya fue solicitada, pronto llegará tú mesa...</font>
                        </h1>
                    </div>
                </div>

                <br><br>

                <table class="table table-danger table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Platos</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Estado</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                               
                        $unidadesTotal=0;
                        //Para calcular cuantos platos pidio en esa orden
                        foreach ($arregloBandeja as $bandejaActual)
                        {
                            $unidadesTotal += $bandejaActual -> getCantidad_und();
                        }
                        
                        //Muestro los datos de la orden que necesito:
                        echo "<tr>";
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getIdOrden() . "</font></h5></td>";
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getFecha_hora() . "</font></h5></td>";
                            echo "<td class='text-center'><font face='Arial' size='3' color='black'>" . $orden->getValor_total() . "</font></td>";
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SOLICITADA <font size='4'><i class='fas fa-clipboard-check'></i></font></font></h5></td>";
                        echo "</tr>";            	      	                   	                         	
                	?>
                    </tbody>
                </table>

                <br><br>

                <!-- Para avisar cuando se envien las observaciones -->
                <?php
                    if(isset($_POST["enviar"]))
                    { 
                ?>  
                    <section id="alert2"> 
                        <div class="alerta">        
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Las observaciones se han enviado correctamente!</strong> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div> 
                    </section>  
                    <br><br>        
                <?php 
                    }
                ?>

                <?php 
                    if ($orden -> getObs() == "")
                    {
                ?>

                <div class="card">
                    <div class="card-header">
                        <h3 style="color: white;" class="text-center">Observaciones</h3>
                    </div>
                    <form
                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/factura.php") . "&m"?>
                        method="post" enctype="multipart/form-data">

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <input type="text" name="observaciones" class="form-control"
                                    placeholder="¿Quieres añadir algo más a tú orden?" required="required">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <input type="submit" name="enviar" value="Enviar" class="btn login_btn btn-block">
                        </div>
                    </form>
                </div>
                <?php 
                    }
                ?>
            </div>

            <?php 
                }
                else if ($orden->getEstado() == 1) //Si la orden esta en estado seleccionando
                {
            ?>
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Factura Generada</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>
                <table class="table table-warning table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Imagen</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Cantidad</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Nombre de Plato</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Precio Unitario</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Total</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                    $valorTotal=0;
                	foreach ($arregloBandeja as $registroActual)
                	{      	    
                	    //Busco los datos del producto de cada registro:
                	    $plato = new Plato ($registroActual->getIdPlato());
                	    $plato -> consultar();
                	    
                	    $valorCantidad = $plato->getPrecio() * $registroActual->getCantidad_und();
                	    $valorTotal += $valorCantidad;
                	    
                	    //Muestro los datos del plato que necesito:
                	    echo "<tr>";
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $plato->getIdPlato() . "</font></h5></td>";             	        
                	        echo "<td class='text-center'><img src='" . $plato->getFoto() . "' width='100px' height='100px'></td>";  
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getCantidad_und() . "</font></h5></td>";
                	        echo "<td ><font face='Arial' size='3' color='black'>" . $plato->getNombre() . "</font></td>";               	        
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>$" . number_format($plato->getPrecio(), ...array(0, ',', '.')) . "</font></h5></td>";
                	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>$" . number_format($valorCantidad, ...array(0, ',', '.')) . "</font></h5></td>";       	       
                	   echo "</tr>";                	   
                	} 
                	echo "<tr>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>Sub-Total:</td></font></h5>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>$" . number_format($valorTotal, ...array(0, ',', '.')) . "</td></font></h5>";
                	echo "</tr>"; 
                	
                	echo "<tr>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td></td>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>Total:</td></font></h5>";
                    	echo "<td><h5><font face='Arial Black' size='4' color='black'>$" . number_format($valorTotal, ...array(0, ',', '.')) . "</td></font></h5>";
                	echo "</tr>"; 
            	?>
                    </tbody>
                </table>
                <br>
                <?php 
                    //Si hay platos en el carrito muestra la factura
                    if ($valorTotal > 0) 
                    {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/realizarOrden.php")?>&m&idOrden=<?php echo $idOrden?>&valorTotal=<?php echo $valorTotal?>"
                            class='btn float-right login_btn3'>
                            REALIZAR ORDEN
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <img src="https://static-unitag.com/images/help/QRCode/qrcode.png?mh=07b7c2a2" width="200px"
                            height="200px" data-toggle="tooltip" title="CODIGO QR">
                    </div>
                </div>
                <?php 
                    }
                ?>
                <br>
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Orden Generada</span></h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>
                <?php 
        		//Traigo el pedido de ese usuario:
        		$orden = new Orden ($idOrden);
        		if ($orden -> consultarUno()) //Si esa orden existe la muestra
        		{
    		?>
                <table class="table table-danger table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Codigo</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Fecha - Hora</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Valor Total</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Platos</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Estado</font>
                                </h5>
                            </th>
                            <th class="text-center">
                                <h5>
                                    <font face='Arial Black' size='4' color='black'>Accion</font>
                                </h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php               	        
                	        $unidadesTotal=0;
                	        //Para calcular cuantos platos pidio en esa orden
                	        foreach ($arregloBandeja as $bandejaActual)
                	        {
                	            $unidadesTotal += $bandejaActual -> getCantidad_und();
                	        }
                	        
                	        //Muestro los datos de la orden que necesito:
                	        echo "<tr>";
                    	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $orden->getIdOrden() . "</font></h5></td>";
                    	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>Esperando...</font></h5></td>";
                    	        echo "<td class='text-center'><font face='Arial' size='3' color='black'>Esperando...</font></td>";
                    	        echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $unidadesTotal . " Unidades" . "</font></h5></td>";
                    	        
                    	        if ($orden->getEstado() == 1)//Seleccionando
                    	        {
                    	            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SELECCIONANDO <font size='4'><i class='fas fa-hourglass-half'></i></font></font></h5></td>";
                    	        }
                    	        else//Solicitado
                    	        {
                    	            echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>SOLICITADO <font size='4'><i class='fas fa-clipboard-check'></i></font></font></h5></td>";
                    	        }
                    	        echo "<td class='text-center'><a href='modalOrden.php?idOrden=" . $idOrden . "&eliminar' data-toggle='modal' data-target='#modalPedido'><font size='5' color='red'><i class='fas fa-trash-alt' data-toggle='tooltip' data-placement='bottom' title='Cancelar'></i></font></td>";
                	        echo "</tr>";            	      	                   	                         	
                	?>
                    </tbody>
                </table>
                <?php 
        		    }
        	    ?>
            </div>
            </div>
            <br><br>
            <div class="row justify-content-center grid-group-md text-xl-center">
                <div class="row">
                    <div class="col-md-12 col-xl-12">
                        <div class="group offset-top-50">
                            <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&idOrden=<?php echo $idOrden?>"
                                class='btn float-right login_btn3'>
                                ACTUALIZAR
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <?php 
                } 
                else if ($orden->getEstado() == 6) //Es porque la orden ya fue entregada
                {   
            ?>
            <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">Tú orden ya fue entregada</span>
                        </h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center">
                            <font size="6">Disfruta, buen provecho!</font>
                        </h1>
                    </div>
                </div>
            </div>
            <?php 
                }
                else //Es porque el estado de la orden es 0 y quiere decir que ya no tiene ordenes activas
                {
            ?>
                <!-- Para avisar cuando el pedido se elimine -->
                <?php
                    if(isset($_GET["confirmar"]))
                    { 
                ?>  
                    <section id="alert2">          
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-check-circle"></i> Se ha cancelado la orden correctamente!</strong> 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>          
                <?php 
                    }
                ?>
                <br><br>
             <div class="container wow fadeInUp">
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center"><span class="d-block font-accent big">No hay ninguna orden todavia</span>
                        </h1>
                        <hr class="divider bg-mantis offset-top-30">
                    </div>
                </div>
                <br><br>
                <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                        <h1 class="text-center">
                            <font size="6">¿Qué quieres pedir hoy?</font>
                        </h1>
                    </div>
                </div>
            </div>
            <?php 
                }
            ?>
        </section>
    </section>
</main>


<!--Este es el Modal de Orden: -->

<section id="modalP">
    <div class="modal fade" id="modalPedido" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content"></div>
        </div>
    </div>
</section>

<!--Este JavaScript es para el Modal de Orden: -->
<script>
$('body').on('show.bs.modal', '.modal', function(e) {
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
</script>

<?php 
    if (isset($_GET["eliminar"]))
    { 
?>
<meta http-equiv="refresh" content="0;url=index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m&confirmar"/>
<?php }?>

<?php 
    }
    else
    {
        echo "<h3>ALERTA DE SEGURIDAD No tiene permisos para entrar a esta seccion...</h3>";
    }
}
else //Si no existe sesion:
{
?>
<meta http-equiv="refresh"
    content="0;url=index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>" />
<?php
}
?>