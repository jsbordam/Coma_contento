<?php 
    $cliente = new Usuario($_SESSION["id"]);
    $cliente -> consultar();

    //Primero busco el id del pedido de ese usuario.
    //Para ello, realizo una consulta en la tabla de pedido:
    
    //Para consultar solo la orden que tenga estado = 1:
    $orden = new Orden ("", "", "", "", $cliente->getIdUsuario());
    $orden -> consultar(1);
    
    
    //Para actualizar el numero de productos de una orden que hay en la bandeja:
    $bandeja = new Bandeja ($orden->getIdOrden());
    $bandeja -> sumarProductos();
?>

<!--==========================
      Sección de encabezado en menu
      ============================-->

<header class="page-head slider-menu-position">
    <!-- RD Navbar Transparent (Menu transparente)-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-default rd-navbar-transparent" data-auto-height="true"
            data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                    <!--Navbar Brand-->
                    <div class="rd-navbar-brand"><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php") ?>&m">
                            <font face="Arial black" size="6" color="white"><i class="fas fa-utensils"></i> CC</font>
                            <font face="Brush Script MT" size="6" color="white">Restaurant</font>
                        </a></div>
                </div>
                <div class="rd-navbar-menu-wrap">
                    <div class="rd-navbar-nav-wrap">
                        <div class="rd-navbar-mobile-scroll">
                            <!--Navbar Brand Mobile-->
                            <div class="rd-navbar-mobile-brand"><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php") ?>&m">
                                        <font face="Arial black" size="6" color="white"><i class="fas fa-utensils"></i>
                                            CC</font>
                                        <font face="Brush Script MT" size="6" color="white">Restaurant</font>
                                    </a></div>
                            <div class="form-search-wrap">
                                <form class="form-search" action="#" method="GET">
                                    <div class="form-group">
                                        <label class="form-label form-search-label form-label-sm"
                                            for="rd-navbar-form-search-widget">Buscar</label>
                                        <input
                                            class="form-search-input input-sm form-control form-control-gray-lightest input-sm"
                                            id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off" />
                                    </div>
                                    <button class="form-search-submit" type="submit"><span
                                            class="mdi mdi-magnify"></span></button>
                                </form>
                            </div>
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                <li class="active"><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/cliente/sesionCliente.php") ?>&m"><span>Inicio</span></a>
                                </li>
                                <li><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/Carta.php") ?>&m"><span>Carta</span></a>
                                </li>
                                <li><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m"><span>Factura</span></a>
                                </li>
                                <li><a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/historialOrdenes.php")?>&m&idUsuario=<?php echo $cliente -> getIdUsuario()?>"><span>Historial Ordenes</span></a>
                                </li>
                                <li><a href="index.php?pid=<?php echo base64_encode("presentacion/log.php")?>&m">Log</a>
                                </li>
                                <li><a
                                        href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/inicio.php")?>&session=0">
                                        <span>Cerrar Sesion</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="modalBandeja.php?idOrden=<?php echo $orden -> getIdOrden()?>"
                                        data-toggle="modal" data-target="#modalCarrito">
                                        <div id="actualizarC">
                                            <font size="5">
                                                <i class="fas fa-concierge-bell" data-toggle="tooltip"
                                                    data-placement="bottom" title="Mi Bandeja">
                                                    <?php 
        							                    echo $bandeja->getCantidad_und();
        							                ?>
                                                </i>
                                            </font>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>

<!--Este es el Modal para carrito: -->
<section id="modalC">	
    <div class="modal fade" id="modalCarrito" tabindex="-1"
    	aria-labelledby="exampleModalLabel" aria-hidden="true"> 
    	<div class="modal-dialog">
    		<div class="modal-content" id="modal-content"></div>
    	</div>
    </div>
</section>    

<!--Este JavaScript es para el modal: -->
<script>
$('body').on('show.bs.modal', '.modal', function (e) {
	var link = $(e.relatedTarget);
	$(this).find(".modal-content").load(link.attr("href"));
});
</script>