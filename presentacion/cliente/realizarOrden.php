<?php
    //Como la orden ya se realizo entonces cambia de estado,
    //Entonces busco la orden con ese id y lo cambio a estado = 2 (SOLICITADO)
    //Y ademas le actualizo su valor total final por el cual se factura:
    
    //Fecha - Hora en que se solicita la orden:
    $fecha_hora = date("Y/m/d - H:i:s");

    $orden = new Orden ($_GET["idOrden"], $fecha_hora, $_GET["valorTotal"]);
    $orden -> editar();
?>
<main class="page-content">
    <section class="section-66 section-lg-110">
		<section id="portfolio">
		<div class="container wow fadeInUp">
			<br><br><br>
			<div class="card">
				<div class="row">
					<div class="col-md-12">		
						<h1 class="text-center"><font face="Cursive" color="white" size="6px"><i class="fas fa-check-square"></i> Tú pedido se ha realizado con exito!</font></h1>
						<br>
						<h1 class="text-center"><font face="Arial Black" color="white" size="5px">Tu orden será llevada a la mesa pronto...</font></h1>				
					</div>		
				</div>	
				<br>
				<div class="row">
					<div class="col-md-6">		
						<img src="https://www.menuspararestaurantes.com/wp-content/uploads/2015/10/mesero-altamente-efectivo.jpg" width="500px" height="500px">					
					</div>		
					<div class="col-md-6">			
						<img src="https://image.freepik.com/foto-gratis/camarero-sirviendo-movimiento-servicio-restaurante-mesero-lleva-platos_109285-5787.jpg" width="500px" height="500px">							
					</div>			
				</div>
				
				<br><br><br><br>
				<div class="row">
					<div class="col-md-12">			
						<h3 class="text-center"><font face="Arial Black" color="white" size="5px">Buen provecho estimado cliente.</font></h3>				
					</div>	
				</div>
			</div>			
			<br><br><br><br>
		</div>
		</section>
	</section>
</main>

<!--==========================
  Footer
============================-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					&copy; Copyright <strong>JONATHAN BORDA MONTOYA</strong>. All
					Rights Reserved
				</div>
			</div>
		</div>
	</div>
</footer>

<meta http-equiv="refresh" content="5;url=index.php?pid=<?php echo base64_encode("presentacion/cliente/factura.php")?>&m"/>