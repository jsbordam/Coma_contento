<?php 
    $plato = new Plato ($_GET["idPla"]);
    $plato -> consultar();
     
   
    echo "<main class='page-content'>";
        echo "<section class='section-66 section-lg-110'>";
            echo "<section id='portfolio'>";
                echo "<div class='container wow fadeInUp'>";
                    echo "<div class='row justify-content-xl-center'>";
                        echo "<div class='col-xl-12'>";
                            echo "<h1 class='text-center'><span class='d-block font-accent big'>" . $plato -> getNombre() . "</span></h1>";
                            echo "<hr class='divider bg-mantis offset-top-30'>";
                        echo "</div>";
                    echo "</div>";

                    echo "<div class='row justify-content-xl-center'>";
                        echo "<div class='col-xl-8'>";
                            echo "<h1 class='text-right'><span class='d-block font-accent big'><font size='7'>" . "$" . number_format($plato -> getPrecio(), ...array(0, ',', '.')) . "</font></span></h1>";
                        echo "</div>";
                    echo "</div>";


                    echo "<div class='row mt-2'>";
                        echo "<div class='col-md-5 text-center'>";
                            echo "<img src='" . $plato -> getFoto() . "' width='400px' height='300px'>";
                        echo "</div>";
                        echo "<div class='col-md-7 text-left'>";
                            echo "<br>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-12'>";
                                ?>
                                    <form
                                        action=<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/bandejaPlatos.php") . "&m&idCar=" . $_GET["idCar"] . "&idPla=" . $_GET["idPla"] . "&reg=" . $_GET["reg"]?>
                                        method="post">
                                        <div class='form-group'>
                                            <div class="form-group col-sm-12">
                                                <h4 class="text-center"><b>Horario en el que esta solicitando el plato:</b></h4>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <select id="inputState" class="form-control" name="idHorario">
                                                    <option value="1">Desayuno</option>
                                                    <option value="2">Almuerzo</option>
                                                    <option value="3">Cena</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class='form-group'>
                                            <button type='submit' name="agregar" class='btn float-right login_btn4 btn-block'>
                                                Agregar <i class='fas fa-concierge-bell'></i>
                                            </button>
                                        </div>
                                    </form>
                                <?php       
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                    echo "<br><br>";

                    echo "<div class='row justify-content-center grid-group-md text-xl-center'>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-12 col-xl-12'>";
                                echo "<div class='group offset-top-50'>";
                                    echo "<a class='btn float-right login_btn3'";
                                        echo "href=index.php?pid=" . base64_encode("presentacion/platos/consultarPlatos.php") . "&m&idCar=" . $_GET["idCar"] . "&reg=" . $_GET["reg"] . " class='btn float-right login_btn3'>";
                                        echo "ATRAS</a>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";

                echo "</div>";
            echo "</section> ";
        echo "</section> ";
    echo "</main>"
?>