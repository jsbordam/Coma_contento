<?php 
    //Capuramos el id de la carta de la cual vamos a mostrar los platos
    $idCarta = $_GET["idCar"];

    //Consulto todos los platos de la carta que hayan:
    $plato = new Plato ();
    $platos = $plato -> consultarTodos($idCarta);

    //Consulto los datos de la region:
    $region = new Region ($_GET["reg"]);
    $region -> consultar();

    //Para saber el nombre del chef encargado de la region:
    $chef = new Usuario ($region->getIdUsuario());
    $chef -> consultar();

    $error = 0;
    if (isset($_GET["error"]))
    {
        $error = $_GET["error"];
    }
?>

<main class="page-content">
    <section class="section-66 section-lg-110">
        <section id="portfolio">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">
                            <font face="Goudy Stout" size="6">
                                Platos tipicos de la region <font color="orange"><?php echo $region -> getNombre();?>
                                </font>
                            </font>
                        </h1>
                    </div>
                </div>


                <!-- Primero se valida si existe el error de la sesion inactiva -->
                <?php
                if ($error == 1) 
                {
                ?>
                <br><br>
                <section id="alert1">
                    <div class="alert alert-dismissible fade show" role="alert">
                        <strong>
                            <i class="fas fa-exclamation-triangle"></i> No se pueden añadir platos a la bandeja,
                            primero debes
                            <a href="modalLogin.php" data-toggle="modal" data-target="#modalLog"
                                class="alert-link"><font color="blue">Iniciar Sesion</font></a>
                        </strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </section>
                <?php
                    }
                ?>

                <!-- Para avisar cuando el producto sea agregado al carrito -->
                <?php
                    if(isset($_GET["agregado"]))
                    { 
                ?>
                <br><br>
                <section id="alert2">
                    <div class="alert alert-dismissible fade show" role="alert">
                        <strong><i class="fas fa-check-circle"></i> Genial: Has agregado un plato a tu bandeja
                            correctamente!</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </section>
                <?php 
                    }
                ?>

                <!-- Para avisar cuando no se pueda crear otra orden -->
                <?php
                    if(isset($_GET["ordenNo"]))
                    { 
                ?>
                <br><br>
                <section id="alert1">
                    <div class="alert alert-dismissible fade show" role="alert">
                        <strong><i class="fas fa-exclamation-triangle"></i> Ya tienes una orden activa,
                         no puedes hacer otra en este momento...</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </section>
                <?php 
                    }
                ?>

                <?php 
                    if (isset($_SESSION["id"]) && $_SESSION["rol"] == "Administrador")
                    {
                ?>
                <div class="row">
                <?php		     
                    foreach ($platos as $platoActual)
                    {   			    
                        if ($platoActual -> getIdRegion() == $_GET["reg"])
                        {
                            echo "<div class='col-md-4'>";
                                echo "<br>";
                                echo "<div class='card1'>";
                                        echo "<a class='portfolio-item'
                                                    style='background-image: url(" . $platoActual -> getFoto() . ");'
                                                    href='index.php?pid=" . base64_encode("presentacion/administrador/editarPlato.php") . "&m&idCar=" . $_GET["idCar"] . "&idP=" . $platoActual -> getIdPlato() ."&reg=" . $_GET["reg"] . "'>";
                                    echo "<div class='details'>";
                                        echo "<h4>";
                                            echo "<font>" . $platoActual -> getIdPlato() . "</font>";
                                        echo "</h4>";
                                        echo "<h3 class='card-title'>";
                                            echo "<font size='6'>¡Uniendo el sabor de todo un país!</font>";
                                        echo "</h3>";
                                        echo "<br>";
                                            echo "<h2 class='card-text'>";
                                                echo "<font face='Arial Black'><i class='fas fa-edit'></i> Editar Producto</font>";
                                            echo "</h2>";                                                        
                                    echo "</div>";
                                    echo "</a>";
                                    echo "<div class='card-body'>";
                                        echo "<h3  class='card-title' align='center'>";
                                            echo "<font>" . $platoActual -> getNombre() . "</font>";
                                            echo " <a href='modalReceta.php?idPla=" . $platoActual -> getIdPlato() . "' data-toggle='modal' data-target='#modalReceta'><font size='5'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver receta'></i></font></a>";
                                        echo "</h3>";
                                        echo "<h3 class='card-text2'";
                                        echo "<font>$" . number_format($platoActual -> getPrecio(), ...array(0, ',', '.')) . "</font>";
                                        echo "</h3>";
                                    echo "</div>";
                                echo "</div>";
                            echo "</div>";   		
                        }   			    	  
                    }
                ?>
                </div>
                <?php 
                    }
                    else if (isset($_SESSION["id"]) && $_SESSION["rol"] == "Chef" || isset($_SESSION["id"]) && $_SESSION["rol"] == "Mesero")
                    {
                ?>
                <div class="row">
                <?php		     
                    foreach ($platos as $platoActual)
                    {   			    
                        if ($platoActual -> getIdRegion() == $_GET["reg"])
                        {
                            echo "<div class='col-md-4'>";
                                echo "<br>";
                                echo "<div class='card1'>";
                                        echo "<a class='portfolio-item'
                                                    style='background-image: url(" . $platoActual -> getFoto() . ");'
                                                    href='#'>";
                                    echo "<div class='details'>";
                                        echo "<h4>";
                                            echo "<font>" . $platoActual -> getIdPlato() . "</font>";
                                        echo "</h4>";
                                        echo "<h3 class='card-title'>";
                                            echo "<font size='6'>¡Uniendo el sabor de todo un país!</font>";
                                        echo "</h3>";
                                        echo "<br>";                                                        
                                    echo "</div>";
                                    echo "</a>";
                                    echo "<div class='card-body'>";
                                        echo "<h3  class='card-title' align='center'>";
                                            echo "<font>" . $platoActual -> getNombre() . "</font>";
                                            echo " <a href='modalReceta.php?idPla=" . $platoActual -> getIdPlato() . "' data-toggle='modal' data-target='#modalReceta'><font size='5'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver receta'></i></font></a>";
                                        echo "</h3>";
                                        echo "<h3 class='card-text2'";
                                        echo "<font>$" . number_format($platoActual -> getPrecio(), ...array(0, ',', '.')) . "</font>";
                                        echo "</h3>";
                                    echo "</div>";
                                echo "</div>";
                            echo "</div>";   		
                        }   			    	  
                    }
                ?>
                </div>
                <?php 
                    }
                    else //Es porque el rol de la sesion es (Cliente o visitante)
                    {
                ?>
                <div class="row">
                    <?php	
                        foreach ($platos as $platoActual)
                        {   			    
                            if  ($platoActual -> getIdRegion() == $_GET["reg"])
                            {  			        
                                echo "<div class='col-md-4'>";
                                    echo "<br>";
                                    echo "<div class='card1'>";
                                        echo "<a class='portfolio-item'
                                                    style='background-image: url(" . $platoActual -> getFoto() . ");'
                                                    href='index.php?pid=" . base64_encode("presentacion/cliente/bandejaPlatos.php") . "&m&verificar&idCar=" . $_GET["idCar"] . "&reg=" . $_GET["reg"] . "&idPla=" . $platoActual -> getIdPlato() . "'>";
                                        echo "<div class='details'>";
                                            echo "<h4>";
                                                echo "<font>" . $platoActual -> getIdPlato() . "</font>";
                                            echo "</h4>";
                                            echo "<h3 class='card-title'>";
                                                echo "<font size='6'>¡Uniendo el sabor de todo un país!</font>";
                                            echo "</h3>";
                                            echo "<br>";   
                                            echo "<h2 class='card-text'>";
                                                echo "<font face='Arial Black'><i class='fas fa-cart-arrow-down'></i> Añadir a la bandeja</font>";
                                            echo "</h2>";
                                        echo "</div>";
                                        echo "</a>";
                                        echo "<div class='card-body'>";
                                            echo "<h3 class='card-title' align='center'>";
                                                echo "<font>" . $platoActual -> getNombre() . "</font>";
                                                echo " <a href='modalReceta.php?idPla=" . $platoActual -> getIdPlato() . "' data-toggle='modal' data-target='#modalReceta'><font size='5'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver receta'></i></font></a>";
                                            echo "</h3>";
                                            echo "<h3 class='card-text2'";
                                                echo "<font face='Arial'>$" . number_format($platoActual -> getPrecio(), ...array(0, ',', '.')) . "</font>";
                                            echo "</h3>";
                                        echo "</div>";
                                    echo "</div>";       			        
                                echo "</div>";
                            }        
                        }
    	            ?>
                </div>
                <?php 
                    }
                ?>

                <br><br>

                <?php 
                    if (isset($_SESSION["id"]) && $_SESSION["rol"] == "Administrador")
                    {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="left">
                            <font size="5" style="color: rgb(91, 238, 7) !important;"><i class="fas fa-circle"></i>
                            </font>
                            <font face="Dancing Script" size="6"> Chef encargado de esta región:
                                <mark><?php echo $chef -> getNombre();?></mark>&nbsp;&nbsp;&nbsp; <a
                                    href="index.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEncargado.php")?>&m&idReg=<?php echo $region -> getIdRegion();?>&idCar=<?php echo $_GET["idCar"];?>"><i
                                        class="fas fa-edit" data-toggle="tooltip" data-placement="bottom"
                                        title="Cambiar encargado"></i></a>
                            </font>
                        </h1>
                    </div>
                </div>

                <br><br>

                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a class="btn btn-lg btn-primary"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarCarta.php")?>&m&idCar=<?php echo $_GET["idCar"];?>">Atras</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                    }
                    else
                    {
                ?>
                <div class="row justify-content-center grid-group-md text-xl-center">
                    <div class="row">
                        <div class="col-md-12 col-xl-12">
                            <div class="group offset-top-50">
                                <a class="btn btn-lg btn-primary"
                                    href="index.php?pid=<?php echo base64_encode("presentacion/menuPrincipal/carta.php")?>&m&c">Atras</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                    }
                ?>

            </div>
        </section>
    </section>
</main>


<!--Este es el Modal de Receta: -->

<section id="modalR">
    <div class="modal fade" id="modalReceta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content"></div>
        </div>
    </div>
</section>

<!--Este JavaScript es para el Modal de Receta: -->
<script>
$('body').on('show.bs.modal', '.modal', function(e) {
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
</script>