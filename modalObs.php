<div class="modal-header">
    <h3 class="text-center"><span class="d-block font-accent big">Observaciones</span></h3>
</div>

<div class="modal-body">
    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/chef/cocina.php") . "&m&idOrden=" . $_GET["idOrden"] . ""?> method="post"
        enctype="multipart/form-data">

        <div class="form-row">
            <div class="form-group col-sm-12">
                <input type="text" name="observaciones" class="form-control"
                    placeholder="¿Quieres añadir un comentario?">
            </div>
        </div>
        <br>
        <div class="form-group">
            <input type="submit" name="preparada" value="Finalizar" class="btn login_btn btn-block">
        </div>
    </form>
</div>

<div class="col-md-12">
    <div align="center">
        <a href="#">
            <font size="8" color="red"><i class="far fa-times-circle" data-dismiss="modal"></i></font>
        </a>
    </div>
</div>